﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AsteroidBehaviour : NetworkBehaviour
{

    public GameObject instantiateOnCollision;
    public int power, hitScoreValue, destroyScoreValue, dodgeScoreValue;

    private Rigidbody2D rb2d;
    private NetGameController gameController;
    private NetTargetController tc;

	[SyncVar(hook="OnSetScale")]
	public Vector3 scale;
	[SyncVar]
	public float speed;
	[SyncVar]
	public float tumble;
	[SyncVar]
	public Quaternion angle;

    private void Start()
    {
        gameController = GameObject.FindWithTag("GameController").GetComponent<NetGameController>();
		tc = GetComponent<NetTargetController>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.angularVelocity = tumble;
        rb2d.velocity = -transform.up * speed;
    }

	private void OnSetScale(Vector3 scale){
		this.scale = scale;
		this.transform.localScale += scale;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
		if (!NetworkServer.active)
			return;
        switch (other.tag)
        {
            case "Boundaries":

                break;
			case "Player":
				if (instantiateOnCollision != null) {
					GameObject tmp=Instantiate (instantiateOnCollision, other.transform.position, transform.rotation);
					NetworkServer.Spawn(tmp);
				}

                tc.damage(tc.pv);
                other.gameObject.GetComponent<NetTargetController>().damage(power);
                break;
			case "Projectile":
				if (instantiateOnCollision != null) {
					GameObject tmp=Instantiate (instantiateOnCollision, other.transform.position, transform.rotation);
					NetworkServer.Spawn(tmp);
				}
				tc.damage (other.gameObject.GetComponent<NetProjectileController> ().power);
				if(other.gameObject.GetComponent<NetProjectileController> ().destroyable||
				other.gameObject.GetComponent<NetProjectileController> ().destroyOnCollision)Destroy (other);
                break;
            default:

                break;
        }

    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Boundaries"))
        {
            gameController.addScore(dodgeScoreValue);
            Destroy(gameObject);
        }
    }
}
