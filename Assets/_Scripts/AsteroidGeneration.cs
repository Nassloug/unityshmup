﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class AsteroidGeneration : NetworkBehaviour
{
	public List<GameObject> asteroids = new List<GameObject> ();
    private AsteroidBehaviour asteroidBehaviour;
    public float minSpeed, maxSpeed, minTumble, maxTumble, minX, maxX, y, spawnRate, power;
    private float lastSpawn;

    private AudioSource levelTheme;

    void Start()
    {
        lastSpawn = Time.time;
        levelTheme = GameObject.FindWithTag("GameController").GetComponent<AudioSource>();
    }

	[ServerCallback]
    void Update()
    {
        if (Time.time >= lastSpawn)
        {
			
            float[] spectrum = new float[512];
            levelTheme.GetSpectrumData(spectrum, 0, FFTWindow.Blackman);
            float avg = 0;
            float idx = 0;
            for (int i = 0; i < spectrum.Length - 1; i++)
            {
                avg += spectrum[i];
                if (i % 8 == 0)
                {
                    avg /= 8;
                    avg *= 1000;
                    Debug.DrawLine(new Vector3(idx, 0, 1), new Vector3(idx, avg, 1), Color.green);

                    Debug.DrawLine(new Vector3(idx + 0.5f, 0, 1), new Vector3(idx + 0.5f, avg, 1), Color.green);
                    Debug.DrawLine(new Vector3(idx, avg, 1), new Vector3(idx + 0.5f, avg, 1), Color.red);
                    avg = 0; idx += 0.5f;
                }
            }
			
            lastSpawn += 1F / spawnRate;

            float randSize = Random.value, randPosition = Random.value, randColor = Random.value, randTumble = Random.value;
			GameObject clone = Instantiate(asteroids[(int)(randColor*asteroids.Count)], new Vector3(Mathf.Clamp(minX + randPosition * (maxX-minX), minX, maxX), y, 1+10*randPosition), Quaternion.identity);
            asteroidBehaviour = clone.GetComponent<AsteroidBehaviour>();
            
            asteroidBehaviour.speed = Mathf.Clamp(minSpeed + (1-randSize) * (maxSpeed-minSpeed), minSpeed, maxSpeed);
            asteroidBehaviour.tumble = Mathf.Clamp(minTumble + randTumble * (maxTumble-minTumble), minTumble, maxTumble);
            asteroidBehaviour.power = (int)(asteroidBehaviour.speed / maxSpeed * power);
			asteroidBehaviour.scale = new Vector3(randSize, randSize, 0);

            float minHue = 0f, maxHue = 1f, minSaturation = 0f, maxSaturation = 0.25f, minValue = 0.75f, maxValue = 1f;
            clone.GetComponent<SpriteRenderer>().color =
                Color.HSVToRGB(Mathf.Clamp(minHue + randColor * (maxHue-minHue), minHue, maxHue),
                               Mathf.Clamp(minSaturation + randColor * (maxSaturation-minSaturation), minSaturation, maxSaturation),
                               Mathf.Clamp(minValue + randColor * (maxValue-minValue), minValue, maxValue));

			NetTargetController tc = clone.GetComponent<NetTargetController>();
			int pv = (int)(tc.pv*randSize*100);
			tc.pv=(pv>1)?pv:1;
			NetworkServer.Spawn(clone);
        }

    }
}
