﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMController : MonoBehaviour {

    public List<AudioClip> audioClips;
    public List<AudioClip> audioLoops;

    public AudioSource source;
    public AudioSource sourceLoop;

    private bool transition,loop;

	// Use this for initialization
	void Start () {
        transition = false;
        if (source != null)
        {
            source.volume = (Parameters.overallVolume / 100f) * (Parameters.bgmVolume / 100f);
            source.loop = false;
        }
        if (sourceLoop != null)
        {
            sourceLoop.volume = (Parameters.overallVolume / 100f) * (Parameters.bgmVolume / 100f);
            sourceLoop.loop = true;
        }
        if (audioLoops[0] != null)
        {
            StartCoroutine(PlayLoop(0));
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnEnable()
    {
        Parameters.OnChange += UpdateVolume;
    }

    private void OnDisable()
    {
        Parameters.OnChange -= UpdateVolume;
    }

    private void UpdateVolume()
    {
        if (source != null && !transition)
        {
            source.volume = (Parameters.overallVolume / 100f) * (Parameters.bgmVolume / 100f);
        }
        if (sourceLoop != null && !transition)
        {
            sourceLoop.volume = (Parameters.overallVolume / 100f) * (Parameters.bgmVolume / 100f);
        }
    }

    public void ChangeBGM(AudioClip newClip, int index, float length, float silence, float speed)
    {
        StopAllCoroutines();
        StartCoroutine(Transition(newClip,index,length,silence,speed));
    }

    public void StopClip()
    {
        source.Stop();
        sourceLoop.Stop();
    }

    public void PlayClip()
    {
        source.Play();
    }

    public void PauseClip()
    {
        source.Pause();
        sourceLoop.Pause();
    }

    public void UnPauseClip()
    {
        source.UnPause();
        sourceLoop.UnPause();
    }

    IEnumerator Transition(AudioClip newClip, int index, float length, float silence, float speed)
    {
        transition = true;
        if (length > 0)
        {
            while (source.volume > 0)
            {
                source.volume -= Time.unscaledDeltaTime / length;
                sourceLoop.volume -= Time.unscaledDeltaTime / length;
                yield return null;
            }
        }
        else
        {
            source.volume = 0;
            sourceLoop.volume = 0;
        }
        yield return new WaitForSeconds(silence);
        if (newClip != null)
        {
            source.clip = newClip;
        }
        else
        {
            source.clip = audioClips[index];
        }
        if (audioLoops[index] != null)
        {
            StartCoroutine(PlayLoop(index));
        }
        else
        {
            sourceLoop.Stop();
            source.Play();
        }
        if (speed > 0)
        {
            while (source.volume < (Parameters.overallVolume / 100f) * (Parameters.bgmVolume / 100f))
            {
                source.volume += Time.unscaledDeltaTime * speed;
                sourceLoop.volume += Time.unscaledDeltaTime * speed;
                yield return null;
            }
        }
        transition = false;
        UpdateVolume();
    }

    IEnumerator PlayLoop(int index)
    {
        sourceLoop.Stop();
        sourceLoop.clip = audioLoops[index];
        source.Play();
        sourceLoop.PlayScheduled(AudioSettings.dspTime+source.clip.length);
        yield return null;
    }
}
