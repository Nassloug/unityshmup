﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEController : MonoBehaviour {

    public Renderer sync;
    public bool playOnAwake,delayed,onlyParam;
    public float offset;
    public float randomPitch;

    [HideInInspector] public bool playing,breakLoop;

    private AudioSource source;
    private float startTime;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = (Parameters.overallVolume / 100f) * (Parameters.seVolume / 100f);
        source.pitch += Random.Range(-randomPitch, randomPitch);
        breakLoop = false;
        playing = false;
        if(!onlyParam && playOnAwake) {
            SoundManager.soundEffects.Add(source);
        }
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(!onlyParam) {
            if(sync != null) {
                if(sync.enabled && !playing) {
                    SoundManager.soundEffects.Add(source);
                } else if(!sync.enabled && source.isPlaying) {
                    breakLoop = true;
                }
            }
            if(delayed && Time.time - startTime >= offset) {
                SoundManager.soundEffects.Add(source);
            }
        }
    }

    private void OnEnable()
    {
        Parameters.OnChange += UpdateVolume;
    }

    private void OnDisable()
    {
        Parameters.OnChange -= UpdateVolume;
    }

    private void UpdateVolume()
    {
        if (source != null)
        {
            source.volume = (Parameters.overallVolume / 100f) * (Parameters.seVolume / 100f);
        }
    }

    public void TriggerSound() {
        SoundManager.soundEffects.Add(source);
    }
}
