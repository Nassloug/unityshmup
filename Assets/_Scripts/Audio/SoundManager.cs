﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static List<AudioSource> soundEffects;

    private List<AudioSource> browsedEffects;
    private List<AudioClip> triggeredEffects;

	// Use this for initialization
	void Start () {
        soundEffects = new List<AudioSource>();
        browsedEffects = new List<AudioSource>();
        triggeredEffects = new List<AudioClip>();
	}
	
	// Update is called once per frame
	void Update () {
        if(soundEffects.Count > 0) {
            browsedEffects.Clear();
            browsedEffects.AddRange(soundEffects);
            soundEffects.Clear();
            foreach (AudioSource source in browsedEffects)
            {
				if(source==null)continue;
                if (!triggeredEffects.Contains(source.clip))
                {
                    triggeredEffects.Add(source.clip);
                    GameObject newObject = new GameObject(source.clip.name);
                    SoundPlayer sound = newObject.AddComponent<SoundPlayer>();
                    AudioSource newSource = newObject.AddComponent<AudioSource>();
                    CopySource(source, newSource);
                    sound.seControl = source.GetComponent<SEController>();
                }
            }
        }
	}

    private void OnEnable() {
        StopAllCoroutines();
        StartCoroutine(ClearSounds());
    }

    private void OnDisable() {
        StopAllCoroutines();
    }

    IEnumerator ClearSounds() {
        while(true) {
            yield return new WaitForSecondsRealtime(0.1F);
            triggeredEffects.Clear();
        }
    }

    public static void CopySource(AudioSource source, AudioSource receiver) {
        receiver.clip = source.clip;
        receiver.volume = source.volume;
        receiver.pitch = source.pitch;
        receiver.panStereo = source.panStereo;
        receiver.mute = source.mute;
        receiver.maxDistance = source.maxDistance;
        receiver.minDistance = source.minDistance;
        receiver.bypassEffects = source.bypassEffects;
        receiver.bypassListenerEffects = source.bypassListenerEffects;
        receiver.bypassReverbZones = source.bypassReverbZones;
        receiver.dopplerLevel = source.dopplerLevel;
        receiver.outputAudioMixerGroup = source.outputAudioMixerGroup;
        receiver.priority = source.priority;
        receiver.reverbZoneMix = source.reverbZoneMix;
        receiver.rolloffMode = source.rolloffMode;
        receiver.spatialBlend = source.spatialBlend;
        receiver.spread = source.spread;
        receiver.playOnAwake = source.playOnAwake;
        receiver.loop = source.loop;
    }

}
