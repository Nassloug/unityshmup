﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour {

    public AudioSource source;
    public SEController seControl;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        StartCoroutine(PlaySound());
    }
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator PlaySound() {
        if(seControl != null && !seControl.playing) {
            if(source.loop) {
                seControl.playing = true;
                source.Play();
                while(seControl != null && seControl.isActiveAndEnabled && !seControl.breakLoop) {
                    yield return null;
                }
                if(seControl != null && seControl.isActiveAndEnabled) {
                    seControl.breakLoop = false;
                }
                seControl.playing = false;
                Destroy(this.gameObject);
            } else {
                seControl.playing = true;
                source.Play();
                yield return new WaitForSeconds(source.clip.length);
                seControl.playing = false;
                Destroy(this.gameObject);
            }
        } else {
            Destroy(this.gameObject);
        }
    }
}
