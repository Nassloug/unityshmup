﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyUponAnimationEnd : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private AudioSource audioSource;

    void Start()
    {
        animator = GetComponent<Animator>() as Animator;
        spriteRenderer = GetComponent<SpriteRenderer>() as SpriteRenderer;
        audioSource = GetComponent<AudioSource>() as AudioSource;
    }

    //check if animation and sound has finished then destroy object
    void Update()
    {
        AnimatorStateInfo asi = animator.GetCurrentAnimatorStateInfo(0);

        if (asi.normalizedTime >= 1 && !audioSource.isPlaying)
        {
            Destroy(gameObject);
            return;
        }
        if (asi.normalizedTime >= 1)
        {
            spriteRenderer.enabled = false;
        }
    }
}
