﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTransitionEvent : ScriptedEvent {

    public AudioClip newClip;
    public bool stop,play,pause,unpause;
    public int index;
    public float length, silence, speed;

    private BGMController bgm;

    public override IEnumerator Event()
    {
        if (stop)
        {
            bgm.StopClip();
        }else if (play)
        {
            bgm.PlayClip();
        }else if (pause)
        {
            bgm.PauseClip();
        }else if (unpause)
        {
            bgm.UnPauseClip();
        }
        bgm.ChangeBGM(newClip, index, length, silence, speed);
        yield return null;
    }

    // Use this for initialization
    void Start () {
        bgm = FindObjectOfType<BGMController>();
	}
	
}
