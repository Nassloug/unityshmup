﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTransitionEvent : ScriptedEvent {

    public BackgroundController background;
    public float delay;
    public FadeRendering fader;
    public float fadeSpeed;
    public int backgroundIndex;
    public bool keep,transition;

    private void Start()
    {
        if (fader != null)
        {
            fader.gameObject.SetActive(false);
        }
        if(fadeSpeed <= 0)
        {
            fadeSpeed = 1f;
        }
    }

    public override IEnumerator Event()
    {
        if (fader != null)
        {
            fader.gameObject.SetActive(true);
        }
        yield return new WaitForSeconds(delay);
        background.ActivateBackground(backgroundIndex, keep, transition);
        if (fader != null)
        {
            fader.FadeOut(fadeSpeed);
        }
    }

}
