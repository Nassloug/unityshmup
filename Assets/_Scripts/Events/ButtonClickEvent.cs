﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickEvent : ScriptedEvent {

    public Button button;

    public override IEnumerator Event()
    {
        button.onClick.Invoke();
        yield return null;
    }
}
