﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelParametersEvent : ScriptedEvent {

    public Parameters config;

    public MenuTransitionEvent transition;

    public override IEnumerator Event()
    {
        config.ReadData();
        if (transition != null)
        {
            transition.TriggerEvent();
        }
        yield return null;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
