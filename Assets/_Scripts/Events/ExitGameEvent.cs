﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGameEvent : ScriptedEvent {
    public override IEnumerator Event()
    {
        Application.Quit();
        yield return null;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
