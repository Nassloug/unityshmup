﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePlayerEvent : ScriptedEvent {

    public Transform[] playerPos;

    [HideInInspector] public bool unfreeze;

    public override IEnumerator Event()
    {
        int i = 0;
        foreach (PlayerController p in FindObjectsOfType<PlayerController>())
        {
            p.freezePlayer = true;
            if (playerPos != null && playerPos.Length>0)
            {
                if (p.isPlayerOne)
                {
                    StartCoroutine(p.gameObject.GetComponent<TargetController>().MoveToPosition(playerPos[0].position, p.maxSpeed / 5f));
                }
                else if(playerPos.Length>1)
                {
                    StartCoroutine(p.gameObject.GetComponent<TargetController>().MoveToPosition(playerPos[1].position, p.maxSpeed / 5f));
                }
            }
            i++;
        }
        while (!unfreeze)
        {
            yield return null;
        }
        foreach (PlayerController p in FindObjectsOfType<PlayerController>())
        {
            p.freezePlayer = false;
        }
        StopAllCoroutines();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
