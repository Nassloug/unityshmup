﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageChangeEvent : ScriptedEvent {

    public Parameters config;

    public MenuTransitionEvent transition;

    public string newLang;

    public override IEnumerator Event() {
        config.ChangeLanguage(newLang);
        if(transition != null) {
            transition.TriggerEvent();
        }
        yield return null;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
