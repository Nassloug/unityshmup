﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTransitionEvent : ScriptedEvent {

    public MenuController menu,nextMenu;
    public bool unpause;

    public override IEnumerator Event()
    {
        if(nextMenu != null)
        {
            menu.gameObject.SetActive(false);
            nextMenu.gameObject.SetActive(true);
        }
        else
        {
            menu.gameObject.SetActive(false);
            if (unpause)
            {
                GameController.Pause();
            }
        }
        triggered = false;
        menu.selection = false;
        yield return null;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
