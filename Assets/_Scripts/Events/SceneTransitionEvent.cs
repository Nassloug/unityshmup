﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionEvent : ScriptedEvent {

    public string scene;
    public FadeRendering transitionScreen;

    private AudioSource source;

    public override IEnumerator Event()
    {
        if(source != null) {
            source.Play();
        }
        transitionScreen.FadeIn(1);
        yield return new WaitForSecondsRealtime(1);
        SceneManager.LoadSceneAsync(scene);
    }

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
