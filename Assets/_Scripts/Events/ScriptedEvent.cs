﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptedEvent : MonoBehaviour {

    public float offset;
    public bool noTrigger;

    [HideInInspector] public bool triggered;

    public abstract IEnumerator Event();

    public void TriggerEvent()
    {
        if (noTrigger || !triggered)
        {
            triggered = true;
            StartCoroutine(Event());
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
