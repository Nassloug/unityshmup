﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDialogueEvent : ScriptedEvent {

    public DialogueController dialogue;

    public override IEnumerator Event()
    {
        dialogue.gameObject.SetActive(true);
        yield return null;
    }

    // Use this for initialization
    void Start () {
        dialogue.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
