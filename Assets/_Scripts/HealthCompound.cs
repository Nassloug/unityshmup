﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCompound : MonoBehaviour {

    public int health,flickerAmount = 3;
    public GameObject instantiateOnDamage,instantiateOnDeath;
    public float flickeringDuration;

    private bool damageIncome;
    private int incomingDamage,flickerCount;
    private float lastFlickerTime;
    private SpriteRenderer sr;

    // Use this for initialization
    void Start () {
        if(sr == null) {
            sr = GetComponent<SpriteRenderer>();
        }
        damageIncome = false;
        flickerCount = 0;
    }
	
	// Update is called once per frame
	void Update () {
        health -= incomingDamage;
        incomingDamage = 0;

        // Flickering management
        if (sr != null && damageIncome){
            if(!sr.enabled) {
                if(Time.time > lastFlickerTime + flickeringDuration / 2) {
                    sr.enabled = true;
                    flickerCount++;
                    lastFlickerTime = Time.time;
                    if(flickerCount >= flickerAmount) {
                        damageIncome = false;
                        flickerCount = 0;
                    }
                }
            } else {
                if(Time.time > lastFlickerTime + flickeringDuration / 2) {
                    sr.enabled = false;
                    lastFlickerTime = Time.time;
                }
            }
        }
        if (health <= 0){
            uponDeath();
        }
	}

    public void uponDeath()
    {
        Destroy(gameObject);
        if (instantiateOnDeath != null){
            Instantiate(instantiateOnDeath, transform.position, transform.rotation);
        }
        if (tag == "Player"){ //gameover lel
            GameObject.FindWithTag("GameController").GetComponent<GameController>().gameOver();
        }
    }

    public void damage(int amount){

        // Player is invincible while flickering
        if(!damageIncome) {
            damageIncome = true;
            incomingDamage += amount;
            if(amount > 0) {
                if(flickeringDuration > 0) {
                    if(sr != null) {
                        sr.enabled = false;
                        lastFlickerTime = Time.time;
                    }
                }
                if(instantiateOnDamage != null) {
                    Instantiate(instantiateOnDamage, transform.position, transform.rotation);
                }
            }
        }

    }
}
