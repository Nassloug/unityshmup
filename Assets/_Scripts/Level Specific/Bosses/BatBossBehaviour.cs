﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatBossBehaviour : BossBehaviour {

    public TargetLauncher[] targetLaunchers;
    public GameObject laser,body,charging;
    public ParticleSystem particles;
    public Transform startingTransform;
    public float introSpeed;
    public FreezePlayerEvent freezePlayer;
    public WaveController wave;

    private Vector3 startPos;
    private Coroutine flap;
    private Animator flapAnim;

	// Use this for initialization
	void Start () {
        charging.SetActive(false);
        freezePlayer.TriggerEvent();
        StartCoroutine(Introduction());
        flapAnim = body.GetComponent<Animator>();
        startPos = startingTransform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator FlapMove()
    {
        while (true)
        {
            for (int i = 0; i < Application.targetFrameRate*0.5f; i+=(int)Time.timeScale)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - Time.timeScale / Application.targetFrameRate, transform.position.z);
                yield return null;
            }
            for (int i = 0; i < Application.targetFrameRate*0.34875f; i+=(int)Time.timeScale)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + Time.timeScale * (0.5f/0.34875f) / Application.targetFrameRate, transform.position.z);
                yield return null;
            }
            
        }
    }

    IEnumerator Introduction()
    {
        while (transform.position.y > startPos.y)
        {
            transform.position -= Vector3.up * Time.deltaTime * introSpeed;
            yield return null;
        }
        yield return new WaitForSeconds(3F);
        freezePlayer.unfreeze = true;
        StartCoroutine(Pattern());
    }

    IEnumerator Pattern()
    {
        while (true)
        {
            flap = StartCoroutine(FlapMove());
            foreach(TargetLauncher t in targetLaunchers)
            {
                foreach (Pattern p in t.GetComponentsInChildren<Pattern>())
                {
                    p.resumeFire();
                }
            }
            for (int i = 0; i < 2; i++)
            {
                while (transform.position.x > -2.8)
                {
                    transform.position = new Vector3(transform.position.x - Time.deltaTime, transform.position.y, transform.position.z);
                    yield return null;
                }
                yield return new WaitForSeconds(2F);
                while (transform.position.x < 2.8)
                {
                    transform.position = new Vector3(transform.position.x + Time.deltaTime, transform.position.y, transform.position.z);
                    yield return null;
                }
                yield return new WaitForSeconds(2F);
            }
            while (transform.position.x > 0)
            {
                transform.position = new Vector3(transform.position.x - Time.deltaTime, transform.position.y, transform.position.z);
                yield return null;
            }
            foreach (TargetLauncher t in targetLaunchers)
            {
                foreach (Pattern p in t.GetComponentsInChildren<Pattern>())
                {
                    p.stopFire();
                }
            }
            StopCoroutine(flap);
            flapAnim.SetBool("Charging", true);
            charging.SetActive(true);
            particles.Play();
            yield return new WaitForSeconds(6F);
            charging.SetActive(false);
            flapAnim.SetBool("Firing", true);
            laser.SetActive(true);
            yield return new WaitForSeconds(3F);
            laser.SetActive(false);
            flapAnim.SetBool("Charging", false);
            flapAnim.SetBool("Firing", false);
            yield return new WaitForSeconds(1F);
        }
    }

    private void OnDestroy()
    {
        FindObjectOfType<BGMController>().ChangeBGM(null,2, 1, 2, 0);
        freezePlayer.unfreeze = false;
        freezePlayer.TriggerEvent();
        wave.finished = true;
    }
}
