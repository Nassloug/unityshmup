﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingBehaviour : MonoBehaviour {

    public Light lighting;
    public float sparklingSpeed;
    public bool sparkle;

    private int timer;
    private float startIntensity;

	// Use this for initialization
	void Start () {
        timer = 0;
        startIntensity = lighting.intensity;
        lighting.intensity = 0f;
	}
	
	// Update is called once per frame
	void Update () {
        timer ++;
        if(sparkle && sparklingSpeed > 0 && timer >= Application.targetFrameRate / sparklingSpeed) {
            lighting.intensity = Mathf.Abs(lighting.intensity - startIntensity);
            timer = 0;
        }
	}

    public void resetLighting() {
        lighting.intensity = 0;
    }
}
