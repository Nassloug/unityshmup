﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class LoadingDots_AmazingScript : MonoBehaviour {

    public Text loading;
    private int count = 0;
    private float lastUpdate = 0;
    public float delay = 0.1f;
	// Update is called once per frame
	void Update () {
        if (Time.time-lastUpdate>=delay){
            if (count < 3){
                loading.text += '.';
                count++;
            }else{
                count = 0;
                loading.text = loading.text.Split('.')[0];
            }
            lastUpdate = Time.time;
        }
    }
}
