﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizeSoundOnAwake : MonoBehaviour
{
    void Start()
    {
        GetComponent<AudioSource>().panStereo = GetComponent<Transform>().position.x / GameObject.FindWithTag("Boundaries").GetComponent<BoxCollider2D>().size.x / 2;
    }
}
