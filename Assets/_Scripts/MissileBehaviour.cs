﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBehaviour : MonoBehaviour {

	public Transform target;
	public SeekAndDestroy sad;
	public GameObject lockAnimation,impactAnimation;
	public float angleOffsetFromZAxis;

    [HideInInspector] public bool aborted;

	private bool launchASAP = false;
    private GameObject lockAnimationRef;
    private Animator lockAnimator;
    private Vector3 lastFramePosition;

    void Start(){
        aborted = false;
		lockAnimationRef=Instantiate(lockAnimation,target.GetComponent<Collider2D>().bounds.center,target.rotation,target);
        lockAnimator = lockAnimationRef.GetComponent<Animator> ();
	}

	void Update(){
		if (launchASAP) {
			launch ();
		}
	}

	//missile start moving toward his target
	public void launch(){
        //if no target, no use
        /*if (target == null) {
            aborted = true;
			Destroy (this.gameObject);
		}*/
        /*//if missile aint properly locked on target, launch when locked
		if (!lockAnimator.GetCurrentAnimatorStateInfo (0).loop) {
			launchASAP = true;
			return;
		}*/

        GetComponent<GotoTrajectory>().hold = false;

		if (target != null) {

            if (lockAnimationRef != null)
            {
                GetComponent<GotoTrajectory>().target = lockAnimationRef.transform;
                GetComponent<TargetRotationBehaviour>().target = target;
            }

		}
	}

	void OnTriggerEnter2D(Collider2D c2d){
		if (c2d.gameObject.layer == LayerMask.NameToLayer("Targets")) {
			Destroy (lockAnimationRef);
			//sad.missileHit (gameObject,c2d.gameObject);
		}else if (c2d.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
        {
            Destroy(lockAnimationRef);
            //sad.missileHit(gameObject, c2d.gameObject);
        }
    }

	void OnDestroy(){
		Destroy (lockAnimationRef);
        if (!aborted)
        {
            GameObject impact = Instantiate(impactAnimation, transform.position, Quaternion.identity);
            float scaleFactor = 3f;
            impact.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
            impact.GetComponent<Animator>().speed *= 2;
            sad.removeMissile(this.gameObject);
        }
	}

}
