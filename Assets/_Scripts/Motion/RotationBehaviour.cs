﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationBehaviour : MonoBehaviour {

    public float speed;
    public float offset;
    public bool x, y, z;

    private int dx = 0, dy = 0, dz = 0;
    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
        if(x) {
            dx = 1;
        }
        if(y) {
            dy = 1;
        }
        if(z) {
            dz = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(Time.time - startTime - offset >= 0) {
            transform.Rotate(dx * speed * Time.deltaTime, dy * speed * Time.deltaTime, dz * speed * Time.deltaTime);
        }
	}
}
