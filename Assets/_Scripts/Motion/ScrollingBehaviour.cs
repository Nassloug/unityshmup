﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBehaviour : MonoBehaviour {

    public List<Transform> scrollingObjects;
    public float scrollingSpeed;
    public SpriteRenderer transitionObject;

    [HideInInspector] public bool transition;

    private Vector3 startPosition, endPosition;
    private float startToEnddistance;
    private bool downwards,transitionMade;

	// Use this for initialization
	void Start () {
        if(scrollingSpeed >= 0) {
            downwards = true;
        }
        if(scrollingObjects != null) {
            startPosition = scrollingObjects[0].localPosition;
            endPosition = scrollingObjects[scrollingObjects.Count - 1].localPosition;
            startToEnddistance = endPosition.y - startPosition.y;
        }
	}
	
	// Update is called once per frame
	void Update () {
        foreach(Transform t in scrollingObjects) {
            t.localPosition -= Vector3.up * Time.deltaTime * scrollingSpeed;
            if(downwards) {
                if(t.localPosition.y <= endPosition.y) {
                    if(!transitionMade) {
                        if(transition && transitionObject != null && t.GetComponent<SpriteRenderer>() != null) {
                            Debug.Log("hophop");
                            t.GetComponent<SpriteRenderer>().sprite = transitionObject.sprite;
                            transitionMade = true;
                        }
                    } else {
                        t.gameObject.SetActive(false);
                    }
                    t.localPosition -= Vector3.up * startToEnddistance;
                }
            } else {
                if(t.localPosition.y >= startPosition.y) {
                    if(!transitionMade) {
                        if(transition && transitionObject != null && t.GetComponent<SpriteRenderer>() != null) {
                            t.GetComponent<SpriteRenderer>().sprite = transitionObject.sprite;
                            transitionMade = true;
                        }
                    } else {
                        t.gameObject.SetActive(false);
                    }
                    t.localPosition += Vector3.up * startToEnddistance;
                }
            }
        }
	}

    public void invertDirection(){
        downwards = false;
        scrollingSpeed = -scrollingSpeed;
    }

}
