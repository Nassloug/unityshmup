﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRotationBehaviour : MonoBehaviour {

    public Transform target;
	public float angleOffset=0;

    // Use this for initialization
    void Start () {

	}

    // Update is called once per frame
    void Update() {
        if(target != null) {
            transform.up = new Vector2(transform.position.x, transform.position.y) - new Vector2(target.position.x, target.position.y);
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + angleOffset);
        }
    }

}
