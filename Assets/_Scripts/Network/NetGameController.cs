﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetGameController : NetworkBehaviour
{
    public GameObject scoreToken;
    public MenuController escapeMenu;

    public delegate void PauseAction();
    public static event PauseAction OnPause;
    public static bool paused;
	static public NetGameController singleton;

	[SyncVar]
    private bool gameIsOver = false;
    private AudioSource levelTheme;
    public SpriteRenderer gameOverBlackOut;
    public Text gameOverText, continueText, scoreText;
	[SyncVar]
    private int score = 0;

	void Awake()
	{
        singleton = this;
    }

    void Start()
    {
        paused = false;
        Time.timeScale = 1;
        levelTheme = GetComponent<AudioSource>();
		//Debug.Log (levelTheme + "\n" + blackOut + "\n" + scoreText + "\n" + gameOverText + "\n" + continueText);
        addScore(0);

        if (escapeMenu != null)
        {
            escapeMenu.gameObject.SetActive(false);
        }
    }
		
    // Update is called once per frame
	[ServerCallback]
    void Update()
    {
        if (gameIsOver)
        {
            if (levelTheme.pitch > 0.5f) levelTheme.pitch -= 0.01f;
			if (gameOverBlackOut.color.a < 0.75f)
            {
				Color tmp = gameOverBlackOut.color;
                tmp.a += 0.01f;
				gameOverBlackOut.color = tmp;
            }
            if (gameOverText.color.a < 1f)
            {
                Color tmp = gameOverText.color;
                tmp.a += 0.02f;
                gameOverText.color = tmp;
            }
            if (continueText.color.a < 1f)
            {
                Color tmp = continueText.color;
                tmp.a += 0.01f;
                continueText.color = tmp;
            }
            if (Input.GetButtonDown("Submit"))
            {
				ExitGame ();
                //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else if (escapeMenu != null && Input.GetButtonDown("Menu") && !paused)
        {
            escapeMenu.gameObject.SetActive(!escapeMenu.gameObject.activeInHierarchy);
            Pause();
        }	
    }

	void FixedUpdate(){
		scoreText.text = "Score : " + score;
	}

	public void scoring(Vector3 pos,int amount){
		GameObject clone = Instantiate(scoreToken, pos, Quaternion.identity);
		ScoreDoodle scoreDoodle = clone.GetComponentInChildren<ScoreDoodle>();

		NetworkServer.Spawn(clone);

		scoreDoodle.scoreValue = amount;
	}

    public void addScore(int amount)
    {
        score += amount;
    }

    public void gameOver()
    {
        gameIsOver = true;
    }

	public void ExitGame()
	{
		if (NetworkServer.active)
		{
			NetworkManager.singleton.StopServer();
		}
		if (NetworkClient.active)
		{
			NetworkManager.singleton.StopClient();
		}
	}

    public static void Pause()
    {
        Time.timeScale = Mathf.Abs(Time.timeScale - 1);
        paused = !paused;
        if (OnPause != null)
        {
            OnPause();
        }
    }

}
