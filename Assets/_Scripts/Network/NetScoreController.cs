﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetScoreController : MonoBehaviour {

    public int score;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if("Player".Equals(collision.tag)) {
            collision.GetComponent<NetPlayerController>().gainXp(score);
            Destroy(transform.parent.gameObject);
        }
    }
}
