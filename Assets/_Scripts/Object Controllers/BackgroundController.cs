﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {

    public List<GameObject> backgrounds;
    public int activeBackground;

	// Use this for initialization
	void Start () {
        if (backgrounds != null && backgrounds.Count > 0)
        {
            foreach (GameObject g in backgrounds)
            {
                g.SetActive(false);
            }
            backgrounds[activeBackground].SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateBackground(int index, bool keep, bool transition)
    {
        if(transition) {
            backgrounds[activeBackground].GetComponent<ScrollingBehaviour>().transition = true;
        }
        else if(!keep) {
            backgrounds[activeBackground].SetActive(false);
        }
        backgrounds[index].SetActive(true);
        activeBackground = index;
    }
}
