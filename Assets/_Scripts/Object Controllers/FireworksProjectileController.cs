﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworksProjectileController : ProjectileController {

    public NovaLauncher novaLauncher;

    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if(novaLauncher.generated && novaLauncher.lastLauncher.shot)
        {
            Destroy(this.gameObject);
        }
	}

}
