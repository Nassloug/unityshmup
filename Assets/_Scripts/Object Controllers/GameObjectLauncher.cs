﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectLauncher: MonoBehaviour {

    public GameObject model;
    public bool repeatOffSet, oneShot, launcherLinked;
    public float fireRate, offset;
    public int rowRate, delay;

    private int rowCounter;
    private float timer;
    private bool stop, pause;

    // Use this for initialization
    void Start() {
        timer = -delay;
    }

    // Update is called once per frame
    void Update() {
        initFire();
    }

    public void fire() {
        GameObject p = Instantiate(model.gameObject, transform.position, transform.rotation);
        if(launcherLinked) {
            p.transform.parent = transform;
        }
    }

    public void initFire() {
        if(!stop && !pause) {
            if(fireRate == 0 || (timer >= Application.targetFrameRate / fireRate || timer == 0)) {
                fire();
                if(oneShot) {
                    Destroy(this);
                } else {
                    if(repeatOffSet && rowRate != 0 && rowCounter >= rowRate - 1) {
                        timer = -offset;
                        rowCounter = 0;
                    } else {
                        timer = 1;
                        rowCounter++;
                    }
                }
            }
            timer++;
        }
    }

    public void stopFire() {
        stop = true;
        timer = 0;
        rowCounter = 0;
    }

    public void setOffset(float offset) {
        this.offset = offset;
        timer = -delay;
    }

    public void resumeFire() {
        stop = false;
    }

    public void resetTimer() {
        timer = 0;
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.tag.Equals("PlayerBoundaries")) {
            stop = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag.Equals("PlayerBoundaries")) {
            stop = false;
        }
    }

    private void OnEnable() {
        GameController.OnPause += Pause;
    }

    private void OnDisable() {
        GameController.OnPause -= Pause;

    }

    private void Pause() {
        pause = !pause;
    }

}
