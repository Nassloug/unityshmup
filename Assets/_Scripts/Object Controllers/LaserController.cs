﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour {

    public bool enemy;
    public float dps;

    private SpriteRenderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (rend.enabled)
        {
            if (!enemy)
            {
                if (collision.gameObject.layer == LayerMask.NameToLayer("Targets"))
                {
                    collision.GetComponent<TargetController>().damage(dps / Application.targetFrameRate);
                }
            }
            else
            {
                if ("Player".Equals(collision.tag))
                {
                    collision.GetComponent<TargetController>().damage(dps);
                }
            }
        }
    }
}
