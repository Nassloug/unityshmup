﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleController : MonoBehaviour {

    public ProjectileController projectile;
    public Transform projectileLauncher;
    public float fireRate;
    public PlayerController player;

    private int frameCount;
    private bool pause;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (!pause&&!player.freezePlayer)
        {
			if (player.fire1)
            {
                if (projectile != null && frameCount >= (Application.targetFrameRate / fireRate))
                {
                    Instantiate(projectile.gameObject, projectileLauncher.position, Quaternion.identity);
                    frameCount = 0;
                }
                frameCount++;
            }
            else
            {
                frameCount = (int)(Application.targetFrameRate / fireRate);
            }
        }
    }

    private void OnEnable()
    {
        GameController.OnPause += Pause;
    }

    private void OnDisable()
    {
        GameController.OnPause -= Pause;   
    }

    private void Pause()
    {
        pause = !pause;
    }
}
