﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

    public ScrollingBehaviour scrolling;
    public bool downwards,playerOnly,enemyOnly;
    public float offset;

    private int direction;

	// Use this for initialization
	void Start () {
        if(downwards) {
            direction = -1;
        } else {
            direction = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (scrolling != null)
        {
            transform.position += Vector3.up * direction * Time.deltaTime * scrolling.scrollingSpeed;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Projectiles"))
        {
            Destroy(collision.gameObject);
        }
        else if (playerOnly && "PlayerShip".Equals(collision.tag))
        {
            GameObject.FindWithTag("Player").GetComponent<TargetController>().uponDeath();
        }
        else if (enemyOnly && !"Player".Equals(collision.tag) && collision.gameObject.layer == LayerMask.NameToLayer("Targets"))
        {
            collision.GetComponent<TargetController>().uponDeath();
        }
    }
}
