﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]

public class PlayerController: NetworkBehaviour{

    [SyncVar]
    public bool alive = true;
    [SyncVar]
    private int tilt = 0;

    public float speed = 5F;
    public float maxSpeed;
    public float fireRate = 2F;
    public float laserRefillSpeed = 0.1F, shieldRefillSpeed = 0.1F;
    public float laserDischargeSpeed = 0.5F, shieldDischargeSpeed = 0.5F;
    public int score, xp;
    public bool shieldEnabled, rocketEnabled, freezePlayer,isPlayerOne=true,noSecondPlayer=false,isLocalPlayer;
    public Sprite[] sprites;
    public GameObject projectile, shield, lvl1Modules, lvl2Modules, lvl3Modules, lvl4Modules;
    public SelectionUIController secondaryWeapon1, secondaryWeapon2, secondaryWeapon3;
    public Transform projectileLauncher;
    public Transform boundaries;
    public int laserSpeed;
    public SpriteRenderer laserHitRend;
    public LightingBehaviour firingLight;
    public PlayerController playerTwo;
    public GameObject playerTwoHidingPanel;

    [HideInInspector] public int lvl,rocketAmount = 25;
    [HideInInspector] public bool fire1, fire1Down, fire2, fire2Up, fire2Down, fire3, nextDown, previousDown;
    [HideInInspector] public float laserEnergy = 100, shieldEnergy = 100;
    [HideInInspector] public Collider2D boundariesCollider;

    private float initialSpeed;
    private float laserStun = 0, shieldStun = 0;
    private int secondaryWeapon = 0;
    private SpriteRenderer spriteRend;
    private Vector2 moveInput = Vector2.zero;
    [SyncVar]
    private Vector2 movement = Vector2.zero;
    private Vector2 oldMovement;
    private int frameCount = 0;
    private LineRenderer laserRend;
    private Vector3 lastFramePosition;
    private Vector3 playerPositionOffset;
    private TargetController tc;
    private SeekAndDestroy sad;
    private bool pause;

	//buttons
	private float horizontal,vertical;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //help differentiate players
        //GetComponent<SpriteRenderer>().material.color = Color.green;
    }

    public override void OnStartLocalPlayer()
    {
        //help differentiate players
        GetComponent<SpriteRenderer>().material.color = Color.white;
    }

    // Use this for initialization
    void Start() {
        if (!shieldEnabled)
        {
            secondaryWeapon2.transform.gameObject.SetActive(false);
        }
        if (!rocketEnabled)
        {
            secondaryWeapon3.transform.gameObject.SetActive(false);
        }
        pause = false;
        lvl = 0;
        initialSpeed = speed;
        if(spriteRend == null) {
            spriteRend = GetComponent<SpriteRenderer>();
        }
        if(laserRend == null) {
            laserRend = GetComponentInChildren<LineRenderer>();
            laserRend.enabled = false;
        }
        if(sad == null)
        {
            sad = GetComponent<SeekAndDestroy>();
        }
        if(laserHitRend != null) {
            laserHitRend.enabled = false;
        }
        if(boundariesCollider == null) {
            if(boundaries != null) {
                boundariesCollider = boundaries.GetComponent<Collider2D>();
			} else {
				boundaries = GameObject.Find ("PlayerBoundaries").transform;
				boundariesCollider = boundaries.GetComponent<Collider2D> ();
			}
            if(boundariesCollider == null) {
                Debug.LogWarning("No boundaries were found at initialization for " + name);
            }
        }
        if(tc == null) {
            tc = GetComponent<TargetController>();
            if(tc == null) {
                Debug.LogWarning("No TargetController attached to player");
            } else {
                //tc.pv = 1;
                tc.player = true;
            }
        }
        lastFramePosition = transform.position;
    }

    // Update is called once per frame
    void Update() {
        if (!isLocalPlayer){
            return;
        }
        if (freezePlayer) {
            spriteRend.sprite = sprites[0];
        } else {
			if(!pause) {
				//remap
				if (isPlayerOne) {
					horizontal = Input.GetAxis ("Horizontal");
					vertical = Input.GetAxis ("Vertical");
					fire1 = Input.GetButton ("Fire1");
                    fire1Down = Input.GetButtonDown("Fire1");
                    fire2 = Input.GetButton ("Fire2");
					fire2Down = Input.GetButtonDown ("Fire2");
					fire2Up = Input.GetButtonUp ("Fire2");
					fire3 = Input.GetButton ("Fire3");
					nextDown = Input.GetButtonDown ("Next");
					previousDown = Input.GetButtonDown ("Previous");
				} else {
					horizontal = Input.GetAxis ("Horizontal - 2");
					vertical = Input.GetAxis ("Vertical - 2");
					fire1 = Input.GetButton ("Fire1 - 2");
                    fire1Down = Input.GetButtonDown("Fire1 - 2");
                    fire2 = Input.GetButton ("Fire2 - 2");
					fire2Down = Input.GetButtonDown ("Fire2 - 2");
					fire2Up = Input.GetButtonUp ("Fire2 - 2");
					fire3 = Input.GetButton ("Fire3 - 2");
					nextDown = Input.GetButtonDown ("Next - 2");
					previousDown = Input.GetButtonDown ("Previous - 2");
				}
				if (Input.GetButton ("Second Player Join")&&!noSecondPlayer) {
					noSecondPlayer = true;
                    playerTwo.transform.gameObject.SetActive(true);
					playerTwo.isPlayerOne = false;
                    playerTwoHidingPanel.SetActive(false);
				} 

				// Detecting movement input
				moveInput = new Vector2(horizontal, vertical);

                // Clamp position to boundaries
                if(boundariesCollider != null) {
                    if((transform.position.x <= boundariesCollider.bounds.min.x && moveInput.x < 0) || (transform.position.x >= boundariesCollider.bounds.max.x && moveInput.x > 0)) {
                        moveInput.x = 0;
                    }
                    if((transform.position.y <= boundariesCollider.bounds.min.y && moveInput.y < 0) || (transform.position.y >= boundariesCollider.bounds.max.y && moveInput.y > 0)) {
                        moveInput.y = 0;
                    }
                }

                // Setting player sprite
                if(moveInput.x <= -0.35) {
                    spriteRend.sprite = sprites[1];
                } else if(moveInput.x >= 0.35) {
                    spriteRend.sprite = sprites[2];
                } else {
                    spriteRend.sprite = sprites[0];
                }

                // Calculating movement
                movement = moveInput * speed * Time.deltaTime;
                if (oldMovement != movement)
                {
                    Cmd_Move(movement);
                    oldMovement = movement;
                }

                // Detecting fire input
                if (fire1) {
                    if(projectile != null && frameCount >= (Application.targetFrameRate / fireRate)) {
                        GameObject tmp = Instantiate(projectile, projectileLauncher.position, Quaternion.identity);
                        NetworkServer.Spawn(tmp);
                        frameCount = 0;
                    }
                    frameCount++;
				} else if(fire2Down) {
                    frameCount = (int)(Application.targetFrameRate / fireRate);
                    switch(secondaryWeapon) {
                        case 0:
                            StopCoroutine("FireLaser");
                            StartCoroutine("FireLaser");
                            break;
                        case 1:
                            StopCoroutine("ActiveShield");
                            StartCoroutine("ActiveShield");
                            break;
                        case 2:
                            sad.lockAndLoad(Mathf.Min(5,rocketAmount));
                            break;
                    }
				} else if(fire2Up && secondaryWeapon == 2) {
                    sad.launchMissiles();
                } else {
                    frameCount = (int)(Application.targetFrameRate / fireRate);
                }

                // Detecting dash input
				if(fire3) {
                    speed = maxSpeed;
                } else {
                    speed = initialSpeed;
                }

                // Managing secondary weapons energy
                if(shieldEnabled) {
                    if(shieldStun > 0) {
                        shieldStun -= Time.deltaTime;
                        shieldStun = Mathf.Max(0, shieldStun);
                    }
                    if(!shield.activeInHierarchy) {
                        shieldEnergy += shieldRefillSpeed;
                        shieldEnergy = Mathf.Min(100, shieldEnergy);
                    }
                }
                if(laserStun > 0) {
                    laserStun -= Time.deltaTime;
                    laserStun = Mathf.Max(0, laserStun);
                }
                if(!laserRend.enabled) {
                    laserEnergy += laserRefillSpeed;
                    laserEnergy = Mathf.Min(100, laserEnergy);
                }

				if(nextDown) {
                    switch(secondaryWeapon) {
                        case 0:
                            if(shieldEnabled) {
                                secondaryWeapon = 1;
                            } else if(rocketEnabled) {
                                secondaryWeapon = 2;
                            }
                            break;
                        case 1:
                            if(rocketEnabled) {
                                secondaryWeapon = 2;
                            } else {
                                secondaryWeapon = 0;
                            }
                            break;
                        case 2:
                            secondaryWeapon = 0;
                            break;
                    }
				} else if(previousDown) {
                    switch(secondaryWeapon) {
                        case 0:
                            if(rocketEnabled) {
                                secondaryWeapon = 2;
                            } else if(shieldEnabled) {
                                secondaryWeapon = 1;
                            }
                            break;
                        case 1:
                            secondaryWeapon = 0;
                            break;
                        case 2:
                            if(shieldEnabled) {
                                secondaryWeapon = 1;
                            } else {
                                secondaryWeapon = 0;
                            }
                            break;
                    }
                }
                switch (secondaryWeapon)
                {
                    case 0:
                        secondaryWeapon1.selectedAnim.transform.gameObject.SetActive(true);
                        secondaryWeapon2.selectedAnim.transform.gameObject.SetActive(false);
                        secondaryWeapon3.selectedAnim.transform.gameObject.SetActive(false);
                        break;
                    case 1:
                        secondaryWeapon1.selectedAnim.transform.gameObject.SetActive(false);
                        secondaryWeapon2.selectedAnim.transform.gameObject.SetActive(true);
                        secondaryWeapon3.selectedAnim.transform.gameObject.SetActive(false);
                        break;
                    case 2:
                        secondaryWeapon1.selectedAnim.transform.gameObject.SetActive(false);
                        secondaryWeapon2.selectedAnim.transform.gameObject.SetActive(false);
                        secondaryWeapon3.selectedAnim.transform.gameObject.SetActive(true);
                        break;
                }
            }
        }

    }

    void FixedUpdate()
    {
        transform.position = new Vector2(Mathf.Clamp(transform.position.x + movement.x, boundariesCollider.bounds.min.x, boundariesCollider.bounds.max.x), Mathf.Clamp(transform.position.y + movement.y, boundariesCollider.bounds.min.y, boundariesCollider.bounds.max.y));
        spriteRend.sprite = sprites[tilt];
    }

    [Command]
    void Cmd_Tilt(int tilt)
    {
        if (!alive)
            return;
        this.tilt = tilt;
        GetComponent<NetworkTransform>().SetDirtyBit(1);
    }

    [Command]
    void Cmd_Move(Vector2 movement)
    {
        if (!alive)
            return;
        this.movement = movement;
        GetComponent<NetworkTransform>().SetDirtyBit(1);
    }

    [Command]
    void Cmd_Shoot()
    {
        if (!alive)
            return;
        GameObject tmp = Instantiate(projectile, projectileLauncher.position, Quaternion.identity);
        NetworkServer.Spawn(tmp);
    }

    public void gainXp(int xpGain) {
        GameObject.FindWithTag("GameController").GetComponent<GameController>().addScore(xpGain);
        xp += xpGain;
        switch(lvl) {
            case 0:
                if(xp >= 2000) {
                    lvlUp();
                    xp -= 1000;
                    gainXp(0);
                }
                break;
            case 1:
                if(xp >= 3000) {
                    lvlUp();
                    xp -= 5000;
                    gainXp(0);
                }
                break;
            case 2:
                if(xp >= 5000) {
                    lvlUp();
                    xp -= 10000;
                    gainXp(0);
                }
                break;
            case 3:
                if(xp >= 7000) {
                    lvlUp();
                    xp -= 7000;
                    gainXp(0);
                }
                break;
            case 4:
                if(xp >= 11000) {
                    lvlUp();
                    xp -= 11000;
                    gainXp(0);
                }
                break;
            case 5:
                if(xp >= 13000) {
                    lvlUp();
                    xp -= 13000;
                    gainXp(0);
                }
                break;
        }
    }

    private void lvlUp() {
        lvl++;
        tc.regen(1);
        laserEnergy = 100;
        shieldEnergy = 100;
        switch(lvl) {
            case 1:
                lvl1Modules.SetActive(true);
                break;
            case 2:
                lvl2Modules.SetActive(true);
                break;
            case 3:
                lvl3Modules.SetActive(true);
                break;
            case 4:
                lvl4Modules.SetActive(true);
                break;
            case 5:
                // Enable damage bonus
                break;
        }
    }

    public void damage() {
        xp = 0;
        if(lvl > 0) {
            lvl--;
            switch(lvl) {
                case 0:
                    lvl1Modules.SetActive(false);
                    break;
                case 1:
                    lvl2Modules.SetActive(false);
                    break;
                case 2:
                    lvl3Modules.SetActive(false);
                    break;
                case 3:
                    lvl4Modules.SetActive(false);
                    break;
                case 4:
                    // Disable damage bonus
                    break;
            }
        }
    }

    IEnumerator ActiveShield() {

        while(secondaryWeapon == 1 && fire2 && !fire1 && shieldEnergy>0 && shieldStun <= 0 && !pause) {

            shieldEnergy -= shieldDischargeSpeed;

            if(shieldEnergy <= 0) {
                shieldStun = 5*Application.targetFrameRate * Time.deltaTime;
            }

            if(shield != null && !shield.activeInHierarchy) {
                shield.SetActive(true);
            }

            yield return null;

        }

        shield.SetActive(false);

    }

    IEnumerator FireLaser() {

        if(firingLight != null) {
            firingLight.sparkle = true;
        }

        while(secondaryWeapon==0&&fire2&&!fire1&&laserEnergy>0&&laserStun<=0&&!pause) {

            laserRend.enabled = true;

            Collider2D targetCollider;

            laserEnergy -= laserDischargeSpeed;

            if(laserEnergy <= 0) {
                laserStun = 5* Application.targetFrameRate * Time.deltaTime;
            }

            if(transform.position.y == lastFramePosition.y) {
                playerPositionOffset = new Vector3(0, 0, 0);
            } else {
                playerPositionOffset += transform.position - lastFramePosition;
            }

            laserRend.material.mainTextureOffset = new Vector2((-Time.time*laserSpeed+Mathf.Min(0,playerPositionOffset.y)), 0);

            Ray2D ray = new Ray2D(projectileLauncher.position, Vector3.up); // laser center
            Ray2D ray2 = new Ray2D(projectileLauncher.position + new Vector3(0.3f,0), Vector3.up); // laser right
            Ray2D ray3 = new Ray2D(projectileLauncher.position - new Vector3(0.3f,0), Vector3.up); // laser left

            RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, 100, LayerMask.GetMask("Targets","Obstacles"));
            RaycastHit2D rayHit2 = Physics2D.Raycast(ray2.origin, ray2.direction, 100, LayerMask.GetMask("Targets","Obstacles"));
            RaycastHit2D rayHit3 = Physics2D.Raycast(ray3.origin, ray3.direction, 100, LayerMask.GetMask("Targets","Obstacles"));

            laserRend.SetPosition(0, ray.origin);

            if(rayHit) {
                if(rayHit2) {
                    if(rayHit.point.y - transform.position.y > rayHit2.point.y - transform.position.y) {
                        rayHit.point = rayHit2.point - new Vector2(0.3f, 0);
                    }
                }
                if(rayHit3) {
                    if(rayHit.point.y - transform.position.y > rayHit3.point.y - transform.position.y) {
                        rayHit.point = rayHit3.point + new Vector2(0.3f, 0);
                    }
                }
                targetCollider = rayHit.collider;
                laserRend.SetPosition(1, rayHit.point);
                laserHitRend.transform.position = new Vector3(ray.origin.x, rayHit.point.y, laserHitRend.transform.position.z);
                laserHitRend.enabled = true;
            } else if(rayHit2) {
                if(rayHit3) {
                    if(rayHit2.point.y - transform.position.y > rayHit3.point.y - transform.position.y) {
                        rayHit2.point = rayHit3.point + new Vector2(0.6f, 0);
                    }
                }
                targetCollider = rayHit2.collider;
                laserRend.SetPosition(1, rayHit2.point - new Vector2(0.3f, 0));
                laserHitRend.transform.position = new Vector3(ray.origin.x,rayHit2.point.y, laserHitRend.transform.position.z);
                laserHitRend.enabled = true;
            } else if(rayHit3) {
                targetCollider = rayHit3.collider;
                laserRend.SetPosition(1, rayHit3.point + new Vector2(0.3f, 0));
                laserHitRend.transform.position = new Vector3(ray.origin.x, rayHit3.point.y, laserHitRend.transform.position.z);
                laserHitRend.enabled = true;
            } else {
                targetCollider = null;
                laserRend.SetPosition(1, ray.GetPoint(100));
                laserHitRend.enabled = false;
            }

            lastFramePosition = transform.position;

            yield return null;
        }

        laserRend.enabled = false;
        laserHitRend.enabled = false;

        if(firingLight != null) {
            firingLight.sparkle = false;
            firingLight.resetLighting();
        }
    }

    private void OnEnable()
    {
        GameController.OnPause += Pause;

    }

    private void OnDisable()
    {
        GameController.OnPause -= Pause;

    }

    private void Pause()
    {
        pause = !pause;
    }

}

