﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectileController : NetworkBehaviour {

    public int power;
    public bool enemy, absorbable, destroyable, destroyOnCollision, continuousDamage, sync;

    private bool active;
    private SpriteRenderer rend;

	// Use this for initialization
	void Start () {
        active = false;
        rend = GetComponent<SpriteRenderer>();
        if (sync)
        {
            if (rend.enabled)
            {
                active = true;
            }
        }
        else
        {
            active = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (sync)
        {
            if (rend.enabled)
            {
                active = true;
            }
            else
            {
                active = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (active)
        {
            if (!enemy)
            {
                if (collision.gameObject.layer == LayerMask.NameToLayer("Targets"))
                {
                    TargetController target = collision.GetComponent<TargetController>();
                    if (target != null)
                    {
                        target.damage(power);
                        if (destroyOnCollision)
                        {
                            NetworkServer.Destroy(this.gameObject);
                        }
                    }
                }
            }
            else
            {
                if (collision.tag.Equals("Player"))
                {
                    if (!(absorbable && collision.GetComponent<PlayerController>().shield.activeInHierarchy))
                    {
                        collision.GetComponent<TargetController>().damage(1);
                    }
                    if (destroyOnCollision)
                    {
                        NetworkServer.Destroy(this.gameObject);
                    }
                }
                else if (destroyable && collision.tag.Equals("Projectile"))
                {
                    ProjectileController p = collision.GetComponent<ProjectileController>();
                    if (p != null)
                    {
                        if (!(p.enemy && enemy))
                        {
                            NetworkServer.Destroy(this.gameObject);
                        }
                    }
                }
            }
        }
        else if (!sync && collision.tag.Equals("PlayerBoundaries"))
        {
            active = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.tag.Equals("Boundaries")) {
            NetworkServer.Destroy(this.gameObject);
        }
        else if(collision.tag.Equals("PlayerBoundaries")) {
            active = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (continuousDamage)
        {
            if (active)
            {
                if (!enemy)
                {
                    if (collision.gameObject.layer == LayerMask.NameToLayer("Targets"))
                    {
                        TargetController target = collision.GetComponent<TargetController>();
                        if (target != null)
                        {
                            target.damage(power);
                            if (destroyOnCollision)
                            {
                                NetworkServer.Destroy(this.gameObject);
                            }
                        }
                    }
                }
                else
                {
                    if (collision.tag.Equals("Player"))
                    {
                        if (!(absorbable && collision.GetComponent<PlayerController>().shield.activeInHierarchy))
                        {
                            collision.GetComponent<TargetController>().damage(1);
                        }
                        if (destroyOnCollision)
                        {
                            NetworkServer.Destroy(this.gameObject);
                        }
                    }
                    else if (destroyable && collision.tag.Equals("Projectile"))
                    {
                        ProjectileController p = collision.GetComponent<ProjectileController>();
                        if (p != null)
                        {
                            if (!(p.enemy && enemy))
                            {
                                NetworkServer.Destroy(this.gameObject);
                            }
                        }
                    }
                }
            }
            else if (!sync && collision.tag.Equals("PlayerBoundaries"))
            {
                active = true;
            }
        }
    }
}
