﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour {

    public int score;
	public TokenController token;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision) {
		if(token.follow.target!=null&&collision.Equals(token.follow.target.GetComponent<Collider2D>())) {
            collision.GetComponent<PlayerController>().gainXp(score);
            Destroy(transform.parent.gameObject);
        }
    }
}
