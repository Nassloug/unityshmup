﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour {

    public Animator specialEffect;
    public float effectOffset;

    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update () {
        if(Time.time - effectOffset >= startTime) {
            specialEffect.gameObject.SetActive(true);
        }
	}

    private void OnDisable() {
        specialEffect.gameObject.SetActive(false);
    }

    private void OnEnable() {
        startTime = Time.time;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Projectiles")) {
            ProjectileController p = collision.GetComponent<ProjectileController>();
            if(p != null && p.absorbable) {
                Destroy(p.gameObject);
            }
        }
    }
}
