﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TargetController : NetworkBehaviour{

    public int pv, flickerAmount = 3;
    public List<GameObject> instantiateOnDamage, instantiateOnDeath;
    public float flickeringDuration;
    public bool player,destroyParent,invincible;

    private bool damageIncome;
    private Collider2D targetCollider,damageBounds;
    private int flickerCount;
    private float realPv, incomingDamage,lastFlickerTime;
    private SpriteRenderer sr;
    private PlayerController playerController;

    // Use this for initialization
    void Start() {
        if(sr == null) {
            sr = GetComponent<SpriteRenderer>();
        }
        if(player) {
            playerController = GetComponent<PlayerController>();
        }
        damageBounds = GameObject.FindGameObjectWithTag("PlayerBoundaries").GetComponent<Collider2D>();
        targetCollider = GetComponent<Collider2D>();
        damageIncome = false;
        flickerCount = 0;
        realPv = pv;
    }

    // Update is called once per frame
    void Update() {
		realPv -= (invincible)?0:incomingDamage;
        incomingDamage = 0;

        // Flickering management
        if(sr != null && damageIncome) {
            if(!sr.enabled) {
                if(Time.time > lastFlickerTime + flickeringDuration / 2) {
                    sr.enabled = true;
                    flickerCount++;
                    lastFlickerTime = Time.time;
                    if(flickerCount >= flickerAmount) {
                        damageIncome = false;
                        flickerCount = 0;
                    }
                }
            } else {
                if(Time.time > lastFlickerTime + flickeringDuration / 2) {
                    sr.enabled = false;
                    lastFlickerTime = Time.time;
                }
            }
        }
		if(!alive()) {
            uponDeath();
        }
    }

    public void uponDeath() {
        if (destroyParent)
        {
            NetworkServer.Destroy(transform.parent.gameObject);
        }
        NetworkServer.Destroy(gameObject);
        foreach (GameObject g in instantiateOnDeath)
        {
            if (g != null)
            {
                GameObject tmp = Instantiate(g, transform.position, Quaternion.identity);
                NetworkServer.Spawn(tmp);
            }
        }
        if(player) { //gameover lel
            GameObject.FindWithTag("GameController").GetComponent<GameController>().gameOver();
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(!player && gameObject.activeSelf && collision.tag.Equals("Boundaries")) {
            NetworkServer.Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(!player && collision.tag.Equals("Player")) {
            collision.GetComponent<TargetController>().damage(1);
        }
    }

    public void damage(float amount) {
		if ((player && !playerController.freezePlayer) || targetCollider.IsTouching(damageBounds))
        {
            if (!player)
			{
                incomingDamage += amount;
            }

            if (!damageIncome)
            {
                damageIncome = true;

                // Player is invincible while flickering
                if (player)
                {
                    playerController.damage();
                    incomingDamage += amount;
                }

                if (amount > 0)
                {
                    if (flickeringDuration > 0)
                    {
                        if (sr != null)
                        {
                            sr.enabled = false;
                            lastFlickerTime = Time.time;
                        }
                    }
					foreach (GameObject g in instantiateOnDamage){
                        if (g != null){
                            GameObject tmp = Instantiate(g, transform.position, Quaternion.identity);
                            NetworkServer.Spawn(tmp);
                        }
                    }
                }
            }
        }

    }

    public void regen(int amount) {
        realPv += amount;
    }

	public bool alive(){
		return realPv > 0;
	}

    public IEnumerator MoveToPosition(Vector3 newPosition, float speed)
    {
        while (transform.position.x != newPosition.x || transform.position.y != newPosition.y)
        {
            float dx = newPosition.x - transform.position.x;
            float dy = newPosition.y - transform.position.y;
            transform.position = new Vector3(transform.position.x + Time.deltaTime * speed * dx, transform.position.y + Time.deltaTime * speed * dy, transform.position.z);
            yield return null;
        }
    }
}
