﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenController : MonoBehaviour {

    [HideInInspector] public GotoTrajectory follow;

	// Use this for initialization
	void Start () {
        follow = GetComponent<GotoTrajectory>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision) {
		if("Player".Equals(collision.tag)&&follow.target==null) {
            follow.target = collision.gameObject.transform;
        }
    }
}
