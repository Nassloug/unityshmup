﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour {

    public EnemyFormationController[] formations;
    public GameObject[] objects;
    public List<ScriptedEvent> scriptedEvents;
    public float waveOffset;
    public WaveController playAfterWave;

    [HideInInspector] public bool finished;

    private float startTime;

	// Use this for initialization
	void Start () {
        finished = false;
		if(formations == null) {
            formations = GetComponentsInChildren<EnemyFormationController>();
        }
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (playAfterWave!=null && !playAfterWave.finished)
        {
            startTime = Time.time;
        }
        foreach (ScriptedEvent s in scriptedEvents)
        {
            if (!s.triggered && Time.time - startTime - waveOffset >= s.offset)
            {
                s.TriggerEvent();
            }
        }

        foreach (EnemyFormationController f in formations) {
            if (f != null && ! f.gameObject.activeInHierarchy && Time.time - startTime - waveOffset>= f.offset) {
                f.gameObject.SetActive(true);
            }
        }

        foreach (GameObject o in objects)
        {
            if (o != null && !o.gameObject.activeInHierarchy && Time.time - startTime >= waveOffset)
            {
                o.SetActive(true);
            }
        }
    }
}
