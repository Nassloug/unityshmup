﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DefaultPattern : Pattern {

    public override void fire() {
        GameObject p = Instantiate(projectile.gameObject,transform.position,transform.rotation);
        applyTrajectoryModifier(p.GetComponent<LinearTrajectory>());
    }

}
