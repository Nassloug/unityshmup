﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NovaLauncher : MonoBehaviour {

    public Pattern launcher;
    public int launcherAmount, concurrentShotsAmount;
    public float angle, angleOffset, firingOffset, generationOffset;
    public bool explosive;
    public CircleCollider2D cc2d;

    [HideInInspector] public bool generated;
    [HideInInspector] public Pattern lastLauncher;

    private float radius, startTime;

    // Use this for initialization
    void Start() {
        startTime = Time.time;
        radius = cc2d.radius;
        generated = false;
        if(concurrentShotsAmount <= 0) {
            concurrentShotsAmount = 1;
        }
        concurrentShotsAmount = launcherAmount / concurrentShotsAmount;
        if(angle == 0) {
            angle = 2 * Mathf.PI;
        } else {
            angle*=Mathf.Deg2Rad;
        }
        angleOffset *= Mathf.Deg2Rad;
    }

    // Update is called once per frame
    void Update() {
        if(!generated) {
            if(Time.time >= startTime + generationOffset) {
                generateLaunchers();
            }
        }
    }

    public void generateLaunchers() {
        if(launcherAmount > 0) {
            for(int i = 0; i < launcherAmount; i++) {
                Vector3 instantiatePosition = new Vector3(Mathf.Sin(angle * ((float)i / (float)launcherAmount) + angleOffset) * radius, -Mathf.Cos(angle * ((float)i / (float)launcherAmount) + angleOffset) * radius, 0);
                GameObject newLauncher = Instantiate(launcher.gameObject, instantiatePosition + transform.position, transform.rotation);
                newLauncher.transform.SetParent(transform);
                Pattern pattern = newLauncher.GetComponent<Pattern>();
                pattern.setOffset((float)(i % concurrentShotsAmount) * (float)firingOffset);
                if(firingOffset > 0) {
                    pattern.fireRate = Application.targetFrameRate / ((float)(concurrentShotsAmount) * firingOffset);
                }
                if(explosive) {
                    pattern.linearTrajectoryXModifier = instantiatePosition.x;
                    pattern.linearTrajectoryYModifier = instantiatePosition.y;
                }
                pattern.gameObject.SetActive(true);
                if (i == launcherAmount - 1)
                {
                    lastLauncher = pattern;
                }
            }
        }
        generated = true;
    }

}
