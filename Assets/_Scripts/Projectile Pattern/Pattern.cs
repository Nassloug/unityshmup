﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pattern : MonoBehaviour {

    public ProjectileController projectile;
    public bool repeatOffSet, enemy, delay, oneShot;
    public float fireRate, offset, projectileSpeed;
    public int power, rowRate;
    public float linearTrajectoryXModifier;
    public float linearTrajectoryYModifier;

    [HideInInspector] public bool shot;

    private int rowCounter;
    private float timer;
    private bool stop,pause;

    public abstract void fire();

    public void applyTrajectoryModifier(LinearTrajectory projectileTrajectory) {
        if(projectileTrajectory != null && (linearTrajectoryXModifier != 0 || linearTrajectoryYModifier != 0)) {
            Vector2 trajectoryModifier = new Vector2(linearTrajectoryXModifier, linearTrajectoryYModifier);
            trajectoryModifier.Normalize();
            projectileTrajectory.dx = trajectoryModifier.x;
            projectileTrajectory.dy = trajectoryModifier.y;
        }
    }

    public void initFire() {
        if(!stop && !pause) {
            if(fireRate == 0 || (timer >= Application.targetFrameRate / fireRate || timer == 0)) {
                fire();
                shot = true;
                if (oneShot)
                {
                    Destroy(this);
                }
                else
                {
                    if (repeatOffSet && rowRate != 0 && rowCounter >= rowRate - 1)
                    {
                        timer = -offset;
                        rowCounter = 0;
                    }
                    else
                    {
                        timer = 1;
                        rowCounter++;
                    }
                }
            }
            timer++;
        }
    }

    public void stopFire() {
        stop = true;
        timer = 0;
        rowCounter = 0;
    }

    public void setOffset(float offset) {
        this.offset = offset;
        if(delay){
            timer = -offset;
        }
    }

    public void resumeFire() {
        stop = false;
    }

    public void resetTimer() {
        timer = 0;
    }

    // Use this for initialization
    void Start () {
        if(repeatOffSet && rowRate <= 0) {
            Debug.LogWarning("Repeat offset needs row rate greater than 0 for " + name);
        }
        stop = true;
        shot = false;
        timer = -offset;
        rowCounter = 0;
        if(enemy) {
            projectile.enemy = true;
            projectileSpeed = -projectileSpeed;
        }
    }
	
	// Update is called once per frame
	void Update () {
        initFire();
	}

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.tag.Equals("PlayerBoundaries")) {
            stop = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag.Equals("PlayerBoundaries")) {
            stop = false;
        }
    }

    private void OnEnable()
    {
        GameController.OnPause += Pause;
    }

    private void OnDisable()
    {
        GameController.OnPause -= Pause;

    }

    private void Pause()
    {
        pause = !pause;
    }
}
