﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLauncher : MonoBehaviour {

    public Pattern launcher;
    public int launcherAmount, concurrentShotsAmount;
    public float angle, angleOffset, firingOffset, generationOffset;
    public bool explosive;

    private Pattern[] launchers;

    private float radius, startTime;
    private bool generated;

    // Use this for initialization
    void Start() {
        startTime = Time.time;
        radius = GetComponent<CircleCollider2D>().radius;
        generated = false;
        launchers = new Pattern[launcherAmount];
        if(concurrentShotsAmount <= 0) {
            concurrentShotsAmount = 1;
        }
        concurrentShotsAmount = launcherAmount / concurrentShotsAmount;
        if(angle == 0) {
            angle = 2 * Mathf.PI;
        } else {
            angle *= Mathf.Deg2Rad;
        }
        angleOffset *= Mathf.Deg2Rad;
    }

    public void generateLaunchers() {
        if(launcherAmount > 0) {
            for(int i = 0; i < launcherAmount; i++) {
                Vector3 instantiatePosition = new Vector3(Mathf.Sin(angle * ((float)i / (float)(launcherAmount-1)) + angleOffset) * radius, -Mathf.Cos(angle * ((float)i / (float)(launcherAmount - 1)) + angleOffset) * radius, launcher.transform.localPosition.z);
                GameObject newLauncher = Instantiate(launcher.gameObject, transform);
                newLauncher.transform.localPosition = instantiatePosition;
                launchers[i] = newLauncher.GetComponent<Pattern>();
                launchers[i].setOffset((float)(i % concurrentShotsAmount) * (float)firingOffset);
                if(firingOffset > 0) {
                    launchers[i].fireRate = Application.targetFrameRate / ((float)(concurrentShotsAmount) * firingOffset);
                }
                if(explosive) {
                    launchers[i].linearTrajectoryXModifier = instantiatePosition.x;
                    launchers[i].linearTrajectoryYModifier = instantiatePosition.y;
                }
                launchers[i].offset = launcher.offset;
                launchers[i].gameObject.SetActive(true);
            }
            generated = true; 
        }
    }

    // Update is called once per frame
    void Update() {
        if(!generated) {
            if(Time.time >= startTime + generationOffset) {
                generateLaunchers();
            }
        } else {
            if(explosive) {
                foreach(Pattern pattern in launchers) {
                    pattern.linearTrajectoryXModifier = pattern.transform.position.x - transform.position.x;
                    pattern.linearTrajectoryYModifier = pattern.transform.position.y - transform.position.y;
                }
            }
        }
    }
}
