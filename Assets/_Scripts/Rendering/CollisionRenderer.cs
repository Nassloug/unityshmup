﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionRenderer : MonoBehaviour {

    public SpriteRenderer rend;
    public string filter;
    public bool isTag;

	// Use this for initialization
	void Start () {
        rend.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if(isTag) {
            if(filter.Equals(collision.tag)) {
                rend.enabled = true;
            }
        } else {
            if(collision.gameObject.layer == LayerMask.NameToLayer(filter)) {
                rend.enabled = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(isTag) {
            if(filter.Equals(collision.tag)) {
                rend.enabled = false;
            }
        } else {
            if(collision.gameObject.layer == LayerMask.NameToLayer(filter)) {
                rend.enabled = false;
            }
        }
    }
}
