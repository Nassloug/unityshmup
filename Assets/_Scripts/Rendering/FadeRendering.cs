﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeRendering : MonoBehaviour {

    public bool fadeOutOnStart,fadeInOnStart,unscaledTimeRendering,continuousFlickering;
    public float fadingSpeed = 1;
    public int flickeringAmount;

    private SpriteRenderer sr;
    private Color color;

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        color = sr.color;
        if (fadeOutOnStart)
        {
            FadeOut(fadingSpeed);
        }else if (fadeInOnStart)
        {
            FadeIn(fadingSpeed);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void FadeIn(float speed)
    {
        StopAllCoroutines();
        StartCoroutine(Fade(speed, true));
    }

    public void FadeOut(float speed)
    {
        StopAllCoroutines();
        StartCoroutine(Fade(speed, false));
    }

    IEnumerator Fade(float speed, bool fadeIn)
    {
        if (fadeIn)
        {
            while (sr.color.a < 1f)
            {
                if (unscaledTimeRendering)
                {
                    color.a = Mathf.Min(1f, sr.color.a + speed * Time.unscaledDeltaTime);
                }
                else
                {
                    color.a = Mathf.Min(1f, sr.color.a + speed * Time.deltaTime);
                }
                sr.color = color;
                yield return null;
            }
        }
        else
        {
            while (sr.color.a > 0)
            {
                if (unscaledTimeRendering)
                {
                    color.a = Mathf.Max(0, sr.color.a - speed * Time.unscaledDeltaTime);
                }
                else
                {
                    color.a = Mathf.Max(0, sr.color.a - speed * Time.deltaTime);
                }
                sr.color = color;
                yield return null;
            }
        }
        if(continuousFlickering || flickeringAmount > 0)
        {
            flickeringAmount--;
            StartCoroutine(Fade(speed, !fadeIn));
        }
    }
}
