﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDoodle : MonoBehaviour {
    public Sprite[] numbers;
    public List<GameObject> digits;
    public int scoreValue;

    public float digitSpacing = 0.08f;
	// Use this for initialization
	void Start () {
        //decompose score to digits
        List<int> score=new List<int>();
		int tmp = scoreValue;
        while (tmp > 0)
        {
            score.Add(tmp % 10);
            tmp = tmp / 10;
        }
        score.Reverse();

        digits[digits.Count - 1].GetComponent<SpriteRenderer>().sprite = numbers[score[digits.Count - 1]];
        score.RemoveAt(0);
        foreach (int digit in score)
        {
            GameObject clone = Instantiate(digits[digits.Count-1], gameObject.transform);
            clone.transform.parent = gameObject.transform;
            clone.transform.Translate(new Vector3(digitSpacing, 0, 0));
            clone.GetComponent<SpriteRenderer>().sprite= numbers[score[digits.Count - 1]];
            digits.Add(clone);
        }
    }

	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag.Equals("Boundaries")){
			GameObject.FindWithTag("GameController").GetComponent<GameController>().addScore(scoreValue);
			Destroy(transform.parent.gameObject);
		}
	}
}
