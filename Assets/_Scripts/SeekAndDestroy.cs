﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekAndDestroy : MonoBehaviour {

    public Transform launcher;
	public GameObject missile;
    public bool targetRay;
    public LineRenderer projectileLauncher;

    [HideInInspector] public Coroutine targetingRayRoutine;

    private Ray2D ray;
	private List<GameObject> missiles = new List<GameObject> ();
    private List<GameObject> targets = new List<GameObject>();
    private PlayerController player;

	public float rocketFireRate = 1F; private float rocketLastLaunch=0;

    private void Start()
    {
        if (player == null)
        {
            player = GetComponent<PlayerController>();
        }
    }

    //draw a line to 'qty' nearby targets in the layer corresponding to the 'mask'
    private void linkNearby(string mask,int qty){
		List<Collider2D> hitColliders =	new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, Mathf.Infinity,1<<LayerMask.NameToLayer(mask)));
		int i = 0;
		hitColliders.Sort(
			delegate(Collider2D a, Collider2D b){
				return Vector2.Distance(this.transform.position,a.transform.position).CompareTo(Vector2.Distance(this.transform.position,b.transform.position));
		});
		while (i < hitColliders.Count)
		{
			if(i<qty)
				Debug.DrawLine(launcher.position, hitColliders[i].transform.position, Color.green);
			i++;
		}
	}

	//start a lock animation to 'qty' nearby targets in the layer corresponding to the 'mask'
	public void lockNearby(string mask,int qty){
		List<Collider2D> hitColliders =	new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, Mathf.Infinity,1<<LayerMask.NameToLayer(mask)));
		int i = 0;
		hitColliders.Sort(
			delegate(Collider2D a, Collider2D b){
				return Vector2.Distance(this.transform.position,a.transform.position).CompareTo(Vector2.Distance(this.transform.position,b.transform.position));
			});
		
		while (i < hitColliders.Count)
		{
			if (qty>0&&hitColliders[i].transform.position.y>this.transform.position.y){
				lockMissile (hitColliders[i].transform);
				qty--;
			}
			i++;
		}
	}

	//start lock animation from missile on target
	private void lockMissile(Transform target){
		Debug.DrawLine(launcher.position, target.position, Color.green);
		GameObject go=Instantiate (missile,launcher.position,Quaternion.identity);
		go.GetComponent<Collider2D>().enabled = false;
        go.GetComponent<SpriteRenderer>().enabled = false;
		go.GetComponent<MissileBehaviour>().target = target;
		go.GetComponent<MissileBehaviour>().sad = this;
		missiles.Add (go);//new MissileBehaviour (this,target,angleOffsetFromZAxis));
        targets.Add(target.gameObject);

	}

	public void lockAndLoad(int qty) {
        if (qty > 0)
        {
            if (Time.time - rocketLastLaunch >= 1F / rocketFireRate || rocketLastLaunch == 0)
            {
                removeAllMissiles();
                if (targetRay)
                {
                    targetingRayRoutine = StartCoroutine(TargetingRay(qty));
                }
                else
                {
                    lockNearby("Targets", qty);
                }
            }
        }
	}

    public void abortLaunch()
    {
        foreach(GameObject missile in missiles)
        {
            missile.GetComponent<MissileBehaviour>().aborted = true;
            Destroy(missile.gameObject);
        }
        missiles.Clear();
        targets.Clear();
    }

    IEnumerator TargetingRay(int qty)
    {
        int i = 0;
        projectileLauncher.enabled = true;
        projectileLauncher.SetPosition(1, new Vector3(0, ray.GetPoint(100).y, 0));
        while(player.fire2&&!player.fire1&&i<qty)
        {
            ray = new Ray2D(projectileLauncher.transform.position, Vector3.up);
            RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, 100, LayerMask.GetMask("Targets","Obstacles"));
            if (rayHit)
            {
                projectileLauncher.SetPosition(1, new Vector3(0, rayHit.point.y-projectileLauncher.transform.position.y, 0));
                if (rayHit.collider.gameObject.layer == LayerMask.NameToLayer("Targets") && !targets.Contains(rayHit.collider.gameObject) && rayHit.collider.IsTouching(player.boundariesCollider))
                {
                    lockMissile(rayHit.collider.transform);
                    i++;
                }
            }
            else
            {
                projectileLauncher.SetPosition(1, new Vector3(0, ray.GetPoint(100).y, 0));
            }
            yield return null;
        }
        if (player.fire1Down)
        {
            abortLaunch();
        }
        projectileLauncher.enabled = false;
        yield return null;
    }

	public void launchMissiles(){
        targets.Clear();
		if (getSize () > 0) {
			foreach (GameObject go in missiles) {
                go.GetComponent<TrajectoryController>().InitPosition(launcher.position);
				go.GetComponent<Collider2D>().enabled = true;
                go.GetComponent<SpriteRenderer>().enabled = true;
				go.GetComponent<MissileBehaviour>().launch ();
                player.rocketAmount--;
			}
			rocketLastLaunch = Time.time;
		}
	}

	//one of the missiles hit his target and he's reporting back here
	public void missileHit(GameObject miss,GameObject hitted){
		//Debug.Log (miss+" hitted "+hitted);
		Destroy(miss);
	}

	public void removeMissile(GameObject missile){
		missiles.Remove (missile);
	}
	public void removeAllMissiles(){missiles.Clear();}
	public int getSize(){return missiles.Count;}
}
