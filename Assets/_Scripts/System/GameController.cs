﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Networking;

public class GameController : NetworkBehaviour
{
    public GameObject scoreToken,gameOverUI;
    public MenuController escapeMenu;
    public Text[] scoreText, upgradeText;

    public delegate void PauseAction();
    public static event PauseAction OnPause;
    public static bool paused;
    static public GameController singleton;

    private PlayerController player,player2;
    [SyncVar]
    private bool gameIsOver = false;
    private AudioSource levelTheme;
    [SyncVar]
    private int score = 0;

    void Awake(){ 
        singleton = this;
    }

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        if(player!=null)player2 = player.playerTwo;
        paused = false;
        Time.timeScale = 1;
        levelTheme = GetComponent<AudioSource>();
        addScore(0);

        if (escapeMenu != null)
        {
            escapeMenu.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!"".Equals(scoreText[0].text))
        {
            scoreText[0].text = scoreText[0].text.Split(':')[0] + ": " + score;
        }
        if (!"".Equals(upgradeText[0].text))
        {
            if (player == null){
                Start();
            }else{
                upgradeText[0].text = upgradeText[0].text.Split('.')[0] + "." + player.lvl;
            }
        }
        if (player2 != null)
        {
            /*if (!"".Equals(scoreText[1].text))
            {
                scoreText[1].text = scoreText[1].text.Split(':')[0] + ": " + score;
            }*/
            if (!"".Equals(upgradeText[1].text))
            {
                upgradeText[1].text = upgradeText[1].text.Split('.')[0] + "." + player2.lvl;
            }
        }
        if (gameIsOver)
        {
            if (!paused && OnPause != null)
            {
                Time.timeScale = 0;
                paused = true;
                OnPause();
            }
            if (!gameOverUI.activeInHierarchy)
            {
                gameOverUI.SetActive(true);
            }
            if (Input.GetButtonDown("Submit"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else if (escapeMenu != null && Input.GetButtonDown("Menu") && !paused)
        {
            escapeMenu.gameObject.SetActive(!escapeMenu.gameObject.activeInHierarchy);
            Pause();
        }
    }

	public void scoring(Vector3 pos,int amount){
		GameObject clone = Instantiate(scoreToken, pos, Quaternion.identity);
		ScoreDoodle scoreDoodle = clone.GetComponentInChildren<ScoreDoodle>();

        NetworkServer.Spawn(clone);

        scoreDoodle.scoreValue = amount;
	}

    public void addScore(int amount)
    {
        score += amount;
    }

    public void gameOver()
    {
        gameIsOver = true;
    }

    public static void Pause()
    {
        Time.timeScale = Mathf.Abs(Time.timeScale - 1);
        paused = !paused;
        if (OnPause != null)
        {
            OnPause();
        }
    }

}
