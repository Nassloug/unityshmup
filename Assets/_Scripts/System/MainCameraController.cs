﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour {

    private bool rotatedMode, rotatedLeft;
    public Camera mainCam, secondCam;

    [HideInInspector] public Camera workingCam;

    private void Awake() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        Debug.Log(Application.targetFrameRate);
    }

    // Use this for initialization
    void Start () {
        rotatedMode = Parameters.rotatedCamera;
        rotatedLeft = Parameters.rotatedCamera;
        if(mainCam == null) {
            Debug.LogWarning("Main camera is not assigned to Main Camera Controller");
        } else if(secondCam != null){
            InitCamera();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void InitCamera()
    {
        if (rotatedMode)
        {
            if (rotatedLeft)
            {
                secondCam.transform.rotation = Quaternion.identity;
                secondCam.transform.Rotate(new Vector3(0, 0, -90));
            }
            mainCam.enabled = false;
            secondCam.enabled = true;
            workingCam = secondCam;
        }
        else
        {
            secondCam.enabled = false;
            mainCam.enabled = true;
            workingCam = mainCam;
        }
    }

    private void OnEnable()
    {
        Parameters.OnChange += ChangeCamera;
    }

    private void OnDisable()
    {
        Parameters.OnChange -= ChangeCamera;
    }

    private void ChangeCamera()
    {
        if(!(rotatedMode && Parameters.rotatedCamera)) {
            rotatedMode = Parameters.rotatedCamera;
            rotatedLeft = Parameters.rotatedCamera;
            InitCamera();
        }
    }
}
