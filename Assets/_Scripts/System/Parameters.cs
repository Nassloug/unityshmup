﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Parameters : MonoBehaviour {

    private string configFile;
    private string[] config;

    public ButtonController fast, normal, slow, rotatedFalse, rotatedTrue, fullScreenTrue, fullScreenFalse;
    public Slider overallSlider, bgmSlider, seSlider;
    public static float typingSpeed,overallVolume,bgmVolume,seVolume;
    public static bool rotatedCamera, fullscreen;
    public static string lang;

    public delegate void ChangeParameters();
    public static event ChangeParameters OnChange;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        ReadData();
    }

    public void ChangeLanguage(string newLang) {
        switch(newLang) {
            case "FR":
                lang = "FR";
                break;
            default:
                lang = "EN";
                break;
        }
        if(OnChange != null) {
            OnChange();
        }
        WriteData();
    }

    public void ChangeTypingSpeed(int speed)
    {
        switch (speed)
        {
            case 0:
                typingSpeed = 0.01F;
                break;
            case 1:
                typingSpeed = 0.05F;
                break;
            case 2:
                typingSpeed = 0.1F;
                break;
        }
        if (OnChange != null)
        {
            OnChange();
        }
        ShowSelectedAnim();
    }

    public void ChangeOverallVolume(Slider slide)
    {
        overallVolume = slide.value;
        if (OnChange != null)
        {
            OnChange();
        }
    }

    public void ChangeBGMVolume(Slider slide)
    {
        bgmVolume = slide.value;
        if (OnChange != null)
        {
            OnChange();
        }
    }

    public void ChangeSEVolume(Slider slide)
    {
        seVolume = slide.value;
        if (OnChange != null)
        {
            OnChange();
        }
    }

    public void ChangeCamera(bool rotated)
    {
        rotatedCamera = rotated;
        if (OnChange != null)
        {
            OnChange();
        }
        ShowSelectedAnim();
    }

    public void ChangeFullscreen(bool full)
    {
        fullscreen = full;
        if (OnChange != null)
        {
            OnChange();
        }
        ShowSelectedAnim();
    }

    public void ShowSelectedAnim()
    {
        fast.selectedAnim.gameObject.SetActive(false);
        normal.selectedAnim.gameObject.SetActive(false);
        slow.selectedAnim.gameObject.SetActive(false);
        rotatedFalse.selectedAnim.gameObject.SetActive(false);
        rotatedTrue.selectedAnim.gameObject.SetActive(false);
        fullScreenTrue.selectedAnim.gameObject.SetActive(false);
        fullScreenFalse.selectedAnim.gameObject.SetActive(false);
        if(typingSpeed == 0.01F)
        {
            fast.selectedAnim.gameObject.SetActive(true);
        }
        else if(typingSpeed == 0.05F)
        {
            normal.selectedAnim.gameObject.SetActive(true);
        }
        else if(typingSpeed == 0.1F)
        {
            slow.selectedAnim.gameObject.SetActive(true);
        }
        if (rotatedCamera)
        {
            rotatedTrue.selectedAnim.gameObject.SetActive(true);
        }
        else
        {
            rotatedFalse.selectedAnim.gameObject.SetActive(true);
        }
        if (fullscreen)
        {
            fullScreenTrue.selectedAnim.gameObject.SetActive(true);
        }
        else
        {
            fullScreenFalse.selectedAnim.gameObject.SetActive(true);
        }
        overallSlider.value = overallVolume;
        bgmSlider.value = bgmVolume;
        seSlider.value = seVolume;
    }

    public void ReadData()
    {
        string path = "Resources/System/config.cfg";

        StreamReader reader = new StreamReader(path);
        configFile = reader.ReadToEnd();
        reader.Close();
        config = configFile.Split(';');
        typingSpeed = float.Parse(config[0]);
        overallVolume = float.Parse(config[1]);
        bgmVolume = float.Parse(config[2]);
        seVolume = float.Parse(config[3]);
        rotatedCamera = bool.Parse(config[4]);
        fullscreen = bool.Parse(config[5]);
        lang = config[6];
        while (lang.ToCharArray()[0] == '\n' || lang.ToCharArray()[0] == '\r')
        {
            lang = lang.Substring(1);
        }
        while (lang.ToCharArray()[lang.Length-1] == '\n' || lang.ToCharArray()[lang.Length-1] == '\r')
        {
            lang = lang.Remove(lang.Length - 1);
        }
        if (OnChange != null)
        {
            OnChange();
        }
        ShowSelectedAnim();
    }

    public void WriteData()
    {
        configFile = typingSpeed + ";" + overallVolume + ";" + bgmVolume + ";" + seVolume + ";" + rotatedCamera + ";" + fullscreen + ';' + lang;

        string path = "Resources/System/config.cfg";

        StreamWriter writer = new StreamWriter(path, false);
        writer.WriteLine(configFile);
        writer.Close();
    }

}

