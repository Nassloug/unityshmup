﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerator : MonoBehaviour {

    public Trajectory trajectory;
    public float offset, multiplier, duration;

    private float startTime;
    private bool triggered;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
        triggered = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!triggered && Time.time >= startTime + offset)
        {
            StartCoroutine(Acceleration(duration));
        }
	}

    public void Accelerate(float time)
    {
        StartCoroutine(Acceleration(time));
    } 

    IEnumerator Acceleration(float time)
    {
        triggered = true;
        trajectory.MultiplySpeed(multiplier);
        yield return new WaitForSeconds(time);
        trajectory.MultiplySpeed(1f/multiplier);
    }
}
