﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidTrajectory : Trajectory {

    public float size;

    protected override void nextStep() {
        newX = Mathf.Pow(Mathf.Cos(t*speed), 3) * size;
        newY = Mathf.Pow(Mathf.Sin(t*speed), 3) * size;
    }
}
