﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltoidTrajectory : Trajectory {

    public float size;

    protected override void nextStep() {
        newX = (2 * Mathf.Cos(t*speed) + Mathf.Cos(2*t*speed)) * size;
        newY = (2 * Mathf.Sin(t*speed) - Mathf.Sin(2*t*speed)) * size;
    }
}
