﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EightTrajectory : Trajectory {

    public bool vertical;
    public float size;

    protected override void nextStep() {
        if(vertical) {
            newX = Mathf.Sin(2 * t * speed) * (size / 2);
            newY = Mathf.Sin(t * speed) * size;
        } else {
            newX = Mathf.Sin(t * speed) * size;
            newY = Mathf.Sin(2 * t * speed) * (size / 2);
        }
    }
}
