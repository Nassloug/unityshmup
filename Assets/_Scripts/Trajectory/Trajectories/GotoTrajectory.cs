﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotoTrajectory : Trajectory {

	public Transform target;
	public float acceleration=1,maxSpeed=0;
    public bool inertia,hold;
	private float dx=0, dy=0;

	protected override void nextStep() {
        if(!hold) {
            if(target != null) {
                dx = target.position.x - transform.position.x;
                dy = target.position.y - transform.position.y;
                Vector2 v = new Vector2(dx, dy);
                v.Normalize();
                if(Mathf.Abs(dx) <= Time.deltaTime * speed && Mathf.Abs(dy) <= Time.deltaTime * speed) {
                    speed /= acceleration;
                } else {
                    speed *= acceleration;
                }
                if(speed > maxSpeed) speed = maxSpeed;
                dx = v.x;
                dy = v.y;
                newX += Time.deltaTime * speed * dx;
                newY += Time.deltaTime * speed * dy;
            } else if(inertia) // if target is null then the object continues in a straight trajectory
              {
                if(dx == 0 && dy == 0) {
                    dy = 1;
                }
                newX += Time.deltaTime * speed * dx;
                newY += Time.deltaTime * speed * dy;
            }
        }
	}
}
