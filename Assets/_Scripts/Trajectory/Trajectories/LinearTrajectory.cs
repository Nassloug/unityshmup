﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearTrajectory : Trajectory {

    public float dx, dy = 1f;

    protected override void nextStep() {
        newX = t * speed * dx;
        newY = t * speed * dy;
    }

}
