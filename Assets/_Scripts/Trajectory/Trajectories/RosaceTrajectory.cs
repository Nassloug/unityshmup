﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RosaceTrajectory : Trajectory {

    public float size;
    public int k;

    protected override void nextStep() {
        newX = Mathf.Cos(k * t * speed) * Mathf.Sin(t * speed) * size;
        newY = Mathf.Cos(k * t * speed) * Mathf.Cos(t * speed) * size;
    }
}
