﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralTrajectory : Trajectory {

    public float size, expansionSpeed, maxSize;
    public bool spring;

    private float newExpansionSpeed;

    private void Start() {
        if(expansionSpeed < 0) {
            expansionSpeed = 0;
        }
        if(maxSize < size) {
            maxSize = size;
        }
        newExpansionSpeed = expansionSpeed;
    }

    protected override void nextStep() {
        newX = Mathf.Sin(t * speed) * size;
        newY = -Mathf.Cos(t * speed) * size;

        if((size > maxSize && expansionSpeed > 0) || (size < 0 && expansionSpeed < 0)) {
            if(spring) {
                newExpansionSpeed = -expansionSpeed;
                expansionSpeed = 0;
            }
            if(size > maxSize) {
                size = maxSize;
            } else if(size < 0) {
                size = 0;
            }
        } else {
            size += Time.deltaTime * expansionSpeed;
        }

        // Smoothening transition between spring periods
        if(newExpansionSpeed < expansionSpeed) {
            expansionSpeed -= Time.deltaTime;
            if(newExpansionSpeed > expansionSpeed) {
                expansionSpeed = newExpansionSpeed;
            }
        }else if(newExpansionSpeed > expansionSpeed) {
            expansionSpeed += Time.deltaTime;
            if(newExpansionSpeed < expansionSpeed) {
                expansionSpeed = newExpansionSpeed;
            }
        }
    }
}
