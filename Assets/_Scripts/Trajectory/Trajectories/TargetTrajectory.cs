﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTrajectory : Trajectory {

    public Transform target;

    private float dx, dy;

    private void Start() {
        dx = target.position.x - transform.position.x;
        dy = target.position.y - transform.position.y;
    }

    protected override void nextStep() {
        newX = t * speed * dx;
        newY = t * speed * dy;
    }

}
