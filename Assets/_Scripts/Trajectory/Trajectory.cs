﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trajectory: MonoBehaviour{

    public float speed;

    protected float t;

    private float startTime;

    private void Start() {

        // Checking existence of trajectory controller
        if(GetComponent<TrajectoryController>() == null) {
            Debug.LogWarning("No trajectory controller found for " + name);
        }
        startTime = Time.time;
    }

    private void Awake() {
        startTime = Time.time;
    }

    protected float newX = 0, newY = 0;

    protected abstract void nextStep();

    public Vector2 iterate(float offset) {
        t = Time.time - startTime - offset;
        if (t >= 0)
        {
            nextStep();
        }
        return new Vector2(newX, newY);
    }

    public void MultiplySpeed(float multiplier)
    {
        startTime -= t * (1f / multiplier) - t;
        speed *= multiplier;
    }

}
