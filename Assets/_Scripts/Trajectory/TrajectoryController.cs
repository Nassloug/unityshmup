﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryController : MonoBehaviour {

    public float offset;

    [HideInInspector] public bool wait;

    private Trajectory[] trajectories;

    private Vector2 nextPosition;
    private float initX, initY, initZ;
    private bool pause;

	// Use this for initialization
	void Start () {
        pause = false;
        wait = false;
        initX = transform.position.x;
        initY = transform.position.y;
        initZ = transform.position.z;
        trajectories = GetComponents<Trajectory>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!pause)
        {
            if (wait)
            {
                offset += Time.deltaTime;
            }
            else
            {
                nextPosition = new Vector2(initX, initY);
                foreach (Trajectory t in trajectories)
                {
                    nextPosition += t.iterate(offset);
                }
                transform.position = new Vector3(nextPosition.x, nextPosition.y, initZ);
            }
        }
	}

    public void InitPosition(Vector3 initPosition) {
        initX = initPosition.x;
        initY = initPosition.y;
        initZ = initPosition.z;
    }

    private void OnEnable()
    {
        GameController.OnPause += Pause;
    }

    private void OnDisable()
    {
        GameController.OnPause -= Pause;

    }

    private void Pause()
    {
        pause = !pause;
    }
}
