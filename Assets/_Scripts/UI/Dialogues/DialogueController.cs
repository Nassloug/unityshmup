﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour {

    public string path;
    public List<GameObject> actorsSprites;
    public Transform leftPos, rightPos;
    public float typingDelay;
    public FreezePlayerEvent freezePlayer;
    public WaveController wave;

    private Text speechBox;
    private string[] script;
    private string openTag, closeTag, styledText, tagValue, textScript;
    private int scriptIndex;
    private float floatValue;
    private bool timed, typing, typingStyle, command, commandEnd, speedChange, record, recordValue, noSkip, pause;

	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void Update () {
        if ("".Equals(textScript) || textScript == null)
        {
            try
            {
                StreamReader reader = new StreamReader("Resources/Texts/_" + Parameters.lang + "/Dialogues/" + path);
                textScript = reader.ReadToEnd();
                reader.Close();
            }
            catch
            {
                if (!"".Equals(Parameters.lang) && Parameters.lang != null)
                {
                    Debug.Log("Dialogue text not found for " + this);
                }
            }
        }
        else
        {
            if (!timed && !pause && Input.GetButtonDown("Submit"))
            {
                if (!typing)
                {
                    ProcessScript();
                }
                else if (!noSkip)
                {
                    StopAllCoroutines();
                    typing = false;
                    typingStyle = false;
                    script[scriptIndex - 1] = Regex.Replace(script[scriptIndex - 1], "<s=.*?>|<\\/s>", "");
                    speechBox.text = script[scriptIndex - 1];
                    if (script[scriptIndex].StartsWith("time"))
                    {
                        StartCoroutine(TimedSpeech(float.Parse(script[scriptIndex].Split(':')[1])));
                    }
                }
            }
        }
	}

    private void OnEnable()
    {
        if (freezePlayer!=null)
        {
            freezePlayer.TriggerEvent();
        }
        Parameters.OnChange += OnParametersChange;
        GameController.OnPause += Pause;
        speechBox = GetComponent<Text>();
        try
        {
            StreamReader reader = new StreamReader("Resources/Texts/_" + Parameters.lang + "/Dialogues/" + path);
            textScript = reader.ReadToEnd();
            reader.Close();
        }
        catch
        {
            if (!"".Equals(Parameters.lang) && Parameters.lang != null)
            {
                Debug.Log("Dialogue text not found for " + this);
            }
        }
        script = textScript.Split(';');
        scriptIndex = 0;
        floatValue = typingDelay;
        timed = false;
        typing = false;
        typingStyle = false;
        command = false;
        record = false;
        recordValue = false;
        noSkip = false;
        pause = false;
        openTag = closeTag = styledText = "";
        foreach (GameObject actor in actorsSprites)
        {
            actor.gameObject.SetActive(false);
        }
        for (int i = 0; i < script.Length; i++)
        {
            if (!"".Equals(script[i]))
            {
                while (script[i].ToCharArray()[0] == '\n' || script[i].ToCharArray()[0] == '\r')
                {
                    script[i] = script[i].Substring(1);
                }
                script[i].Trim();
            }
        }
        ProcessScript();
    }

    private void OnDisable()
    {
        Parameters.OnChange -= OnParametersChange;
        GameController.OnPause -= Pause;

        End();
    }

    private void ProcessScript()
    {
        Debug.Log(script[scriptIndex]);
        if (script[scriptIndex].StartsWith("actor"))
        {
            SetActor(int.Parse(script[scriptIndex].Split(':')[1].Trim()), script[scriptIndex].Split(':')[2].Trim());
            scriptIndex++;
            ProcessScript();
        }
        else if ("noskip".Equals(script[scriptIndex])){
            noSkip = true;
            scriptIndex++;
            ProcessScript();
        }
        else if ("skip".Equals(script[scriptIndex])){
            noSkip = false;
            scriptIndex++;
            ProcessScript();
        }
        else if ("end".Equals(script[scriptIndex]))
        {
            End();
        }
        else
        {
            StartCoroutine(TypeChars(script[scriptIndex]));
            scriptIndex++;
        }
    }

    private void SetActor(int idx, string position)
    {
        actorsSprites[idx].SetActive(true);
        if ("left".Equals(position))
        {
            actorsSprites[idx].transform.position = leftPos.position;
            actorsSprites[idx].transform.localScale = leftPos.localScale;
        }
        else if ("right".Equals(position))
        {
            actorsSprites[idx].transform.position = rightPos.position;
            actorsSprites[idx].transform.localScale = rightPos.localScale;
        }
    }

    private void End()
    {
        StopAllCoroutines();
        if (freezePlayer!=null)
        {
            freezePlayer.unfreeze = true;
        }
        foreach (GameObject actor in actorsSprites)
        {
            actor.gameObject.SetActive(false);
        }
        if (wave != null)
        {
            wave.finished = true;
        }
        gameObject.SetActive(false);
    }

    IEnumerator TimedSpeech(float duration)
    {
        timed = true;
        scriptIndex++;
        yield return new WaitForSeconds(duration);
        timed = false;
        ProcessScript();
    }

    IEnumerator TypeChars(string line)
    {
        typing = true;
        speechBox.text = "";
        foreach(char c in line)
        {
            if (command)
            {
                if (record)
                {
                    if (c == '<')
                    {
                        closeTag += c;
                        commandEnd = true;
                        record = false;
                    }
                    else
                    {
                        styledText += c;
                    }
                }
                else if (commandEnd)
                {
                    closeTag += c;
                    if(c == '>')
                    {
                        commandEnd = false;
                        command = false;
                        StartCoroutine(TypeStyledChars(styledText));
                        yield return new WaitWhile(new System.Func<bool>(() => typingStyle));
                    }
                }
                else
                {
                    openTag += c;
                    if (c == '>')
                    {
                        if (recordValue)
                        {
                            floatValue = float.Parse(tagValue);
                        }
                        recordValue = false;
                        record = true;
                    }
                    else if (recordValue)
                    {
                        tagValue += c;
                    }
                    else if (c == 's') // change typing speed
                    {
                        speedChange = true;
                    }
                    else if(c == '=')
                    {
                        if (speedChange)
                        {
                            recordValue = true;
                            tagValue = "";
                        }
                    }
                }
            }
            else if (c == '<')
            {
                command = true;
                openTag = "" + c;
                closeTag = "";
                styledText = "";
            }
            else
            {
                speechBox.text += c;
                yield return new WaitForSeconds(typingDelay);
            }
        }
        if (script[scriptIndex].StartsWith("time"))
        {
            StartCoroutine(TimedSpeech(float.Parse(script[scriptIndex].Split(':')[1])));
        }
        typing = false;
    }

    IEnumerator TypeStyledChars(string line)
    {
        typingStyle = true;
        foreach (char c in line)
        {
            if (speedChange)
            {
                speechBox.text += c ;
            }
            else
            {
                speechBox.text += openTag + c + closeTag;
            }
            yield return new WaitForSeconds(floatValue);
        }
        floatValue = typingDelay;
        typingStyle = false;
    }

    private void OnParametersChange()
    {
        typingDelay = Parameters.typingSpeed;
    }

    private void Pause()
    {
        pause = !pause;
    }
}
