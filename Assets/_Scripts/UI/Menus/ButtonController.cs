﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

    public SpriteRenderer overAnim, selectedAnim;
    public ScriptedEvent scriptedEvent;
    public ButtonController left, right, top, down, cancel, submit;
    public bool noBlocking;
    public Text description;

	// Use this for initialization
	void Start () {
        //overAnim.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //private void OnMouseOver()
    //{
    //    overAnim.enabled = true;
    //}

    //private void OnMouseExit()
    //{
    //    overAnim.enabled = false;
    //}

    private void OnMouseDown()
    {
        scriptedEvent.TriggerEvent();
    }

}
