﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

    public List<ButtonController> buttons;
    public ScriptedEvent cancelEvent;
    public bool pauseOnCancel;

    public AudioClip submitSound;
    public AudioClip cancelSound;
    public AudioClip navigateSound;
    public AudioSource source;

    [HideInInspector] public bool selection;
    [HideInInspector] public ButtonController activeButton;

    private bool navigating;

	// Use this for initialization
	void Start () {

    }

    private void OnEnable()
    {
        foreach (ButtonController b in buttons)
        {
            b.overAnim.enabled = false;
        }
        activeButton = buttons[0];
        buttons[0].overAnim.enabled = true;
        navigating = false;
        selection = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!selection)
        {
            if (Input.GetAxisRaw("Vertical") < 0)
            {
                if (!navigating)
                {
                    StopAllCoroutines();
                    StartCoroutine(NavigateDown());
                }
            }
            else if (Input.GetAxisRaw("Vertical") > 0)
            {
                if (!navigating)
                {
                    StopAllCoroutines();
                    StartCoroutine(NavigateUp());
                }
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                if (!navigating)
                {
                    StopAllCoroutines();
                    StartCoroutine(NavigateLeft());
                }
            }
            else if (Input.GetAxisRaw("Horizontal") > 0)
            {
                if (!navigating)
                {
                    StopAllCoroutines();
                    StartCoroutine(NavigateRight());
                }
            }
            else
            {
                navigating = false;
            }
        }
        if (Input.GetButtonDown("Submit")) {
            if (activeButton.submit != null)
            {
                foreach (ButtonController b in buttons)
                {
                    b.overAnim.enabled = false;
                }
                activeButton = activeButton.submit;
                activeButton.overAnim.enabled = true;
                source.clip = submitSound;
                source.Play();
            }
            else if(activeButton.scriptedEvent != null)
            {
                if (!activeButton.noBlocking)
                {
                    selection = true;
                }
                source.clip = submitSound;
                source.Play();
                activeButton.scriptedEvent.TriggerEvent();
            }
        }
        else if (Input.GetButtonDown("Cancel"))
        {
            if (activeButton.cancel != null)
            {
                foreach (ButtonController b in buttons)
                {
                    b.overAnim.enabled = false;
                }
                activeButton = activeButton.cancel;
                activeButton.overAnim.enabled = true;
                source.clip = cancelSound;
                source.Play();
            }
            else if(cancelEvent != null)
            {
                source.clip = cancelSound;
                source.Play();
                cancelEvent.TriggerEvent();
            }
            else
            {
                if (pauseOnCancel)
                {
                    source.clip = cancelSound;
                    source.Play();
                    GameController.Pause();
                    gameObject.SetActive(false);
                }
            }
        }
    }

    IEnumerator NavigateDown()
    {
        navigating = true;
        float navDelay = 0.3F;
        while ((Input.GetAxisRaw("Vertical") < 0) && activeButton.down != null && !selection)
        {
            foreach (ButtonController b in buttons)
            {
                b.overAnim.enabled = false;
            }
            activeButton = activeButton.down;
            while (!activeButton.gameObject.activeSelf)
            {
                activeButton = activeButton.down;
            }
            activeButton.overAnim.enabled = true;
            source.clip = navigateSound;
            source.Play();
            yield return new WaitForSecondsRealtime(navDelay);
            navDelay = 0.15F;
        }
        navigating = false;
    }

    IEnumerator NavigateUp()
    {
        navigating = true;
        float navDelay = 0.3F;
        while ((Input.GetAxisRaw("Vertical") > 0) && activeButton.top != null && !selection)
        {
            foreach (ButtonController b in buttons)
            {
                b.overAnim.enabled = false;
            }
            activeButton = activeButton.top;
            while (!activeButton.gameObject.activeSelf)
            {
                activeButton = activeButton.top;
            }
            activeButton.overAnim.enabled = true;
            source.clip = navigateSound;
            source.Play();
            yield return new WaitForSecondsRealtime(navDelay);
            navDelay = 0.15F;
        }
        navigating = false;
    }

    IEnumerator NavigateLeft()
    {
        navigating = true;
        float navDelay = 0.3F;
        while ((Input.GetAxisRaw("Horizontal") < 0) && activeButton.left != null && !selection)
        {
            foreach (ButtonController b in buttons)
            {
                b.overAnim.enabled = false;
            }
            activeButton = activeButton.left;
            while (!activeButton.gameObject.activeSelf)
            {
                activeButton = activeButton.left;
            }
            activeButton.overAnim.enabled = true;
            source.clip = navigateSound;
            source.Play();
            yield return new WaitForSecondsRealtime(navDelay);
            navDelay = 0.15F;
        }
        navigating = false;
    }

    IEnumerator NavigateRight()
    {
        navigating = true;
        float navDelay = 0.3F;
        while ((Input.GetAxisRaw("Horizontal") > 0) && activeButton.right != null && !selection)
        {
            foreach (ButtonController b in buttons)
            {
                b.overAnim.enabled = false;
            }
            activeButton = activeButton.right;
            while (!activeButton.gameObject.activeSelf)
            {
                activeButton = activeButton.right;
            }
            activeButton.overAnim.enabled = true;
            source.clip = navigateSound;
            source.Play();
            yield return new WaitForSecondsRealtime(navDelay);
            navDelay = 0.15F;
        }
        navigating = false;
    }
}
