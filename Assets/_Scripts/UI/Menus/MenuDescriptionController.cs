﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuDescriptionController : MonoBehaviour {

    public MenuController menu;

    private Text menuText;

	// Use this for initialization
	void Start () {
        menuText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (menu.activeButton.description != null)
        {
            menuText.text = menu.activeButton.description.text;
        }
	}
}
