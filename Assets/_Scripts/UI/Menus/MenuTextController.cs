﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MenuTextController : MonoBehaviour {

    public string path;
    public int index;
    public bool noUpdate;

    private Text menuText;
    private string line;
    private string[] fileContent;

	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void Update () {
        if("".Equals(line)||line == null)
        {
            InitText();
        }
    }

    private void InitText()
    {
        if (menuText != null)
        {
            try
            {
                StreamReader reader = new StreamReader("Resources/Texts/_" + Parameters.lang + "/UI/" + path);
                line = reader.ReadToEnd();
                reader.Close();
            }
            catch
            {
                if (!"".Equals(Parameters.lang) && Parameters.lang != null)
                {
                    Debug.Log("UI text not found for " + this);
                }
            }
            if (line != null)
            {
                fileContent = line.Split(';');
            }
            if (fileContent!=null && !"".Equals(fileContent[index]))
            {
                while (fileContent[index].ToCharArray()[0] == '\n' || fileContent[index].ToCharArray()[0] == '\r')
                {
                    fileContent[index] = fileContent[index].Substring(1);
                }
                fileContent[index].Trim();
                menuText.text = fileContent[index];
            }
        }
    }

    private void OnEnable()
    {
        if (menuText == null)
        {
            menuText = GetComponent<Text>();
        }
        InitText();
        Parameters.OnChange += OnParametersChange;
    }

    private void OnDisable()
    {
        Parameters.OnChange -= OnParametersChange;
    }

    private void OnParametersChange()
    {
        if (line == null || "".Equals(line) || !noUpdate)
        {
            InitText();
        }
    }
}
