﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour {

    public ButtonController button;
    public MenuController menu;
    public string playerAttribute;
    public bool player2;

    private Slider slider;
    private bool navigating;
    private bool active;
    private PlayerController player;

	// Use this for initialization
	void Start () {
        active = false;
        navigating = false;
        slider = GetComponent<Slider>();
        if (!"".Equals(playerAttribute))
        {
            player = FindObjectOfType<PlayerController>();
            if (player2)
            {
                player = player.playerTwo;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (player != null)
        {
            if ("lvl".Equals(playerAttribute))
            {
                switch (player.lvl)
                {
                    case 0:
                        slider.maxValue = 2000;
                        break;
                    case 1:
                        slider.maxValue = 3000;
                        break;
                    case 2:
                        slider.maxValue = 5000;
                        break;
                    case 3:
                        slider.maxValue = 7000;
                        break;
                    case 4:
                        slider.maxValue = 11000;
                        break;
                    case 5:
                        slider.maxValue = 13000;
                        break;
                }
                slider.value = player.xp;
            }
            else if ("laser".Equals(playerAttribute))
            {
                slider.maxValue = 100;
                slider.value = player.laserEnergy;
            }
            else if ("shield".Equals(playerAttribute))
            {
                slider.maxValue = 100;
                slider.value = player.shieldEnergy;
            }
            else if("rockets".Equals(playerAttribute)) {
                slider.maxValue = 25;
                slider.value = player.rocketAmount;
            }
        }
        else if("".Equals(playerAttribute) || playerAttribute == null)
        {
            if (menu.activeButton.Equals(button))
            {
                active = true;
            }
            else
            {
                active = false;
            }
            if (active)
            {
                if (slider.direction == Slider.Direction.LeftToRight)
                {
                    if (Input.GetAxisRaw("Horizontal") < 0)
                    {
                        slider.value -= 1;
                    }
                    else if (Input.GetAxisRaw("Horizontal") > 0)
                    {
                        slider.value += 1;

                    }
                }
                else if (slider.direction == Slider.Direction.BottomToTop)
                {
                    if (Input.GetAxisRaw("Vertical") < 0)
                    {
                        slider.value -= 1;
                        slider.onValueChanged.Invoke(0);

                    }
                    else if (Input.GetAxisRaw("Vertical") > 0)
                    {
                        slider.value += 1;
                        slider.onValueChanged.Invoke(0);

                    }
                }
            }
        }
	}

}
