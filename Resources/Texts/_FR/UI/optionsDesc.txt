Contrôle la vitesse à laquelle le texte des dialogues est affiché.;
Contrôle le volume général ainsi que celui des musiques et effets sonores spécifiquement.;
Alterne entre un affichage horizontal classique ou un affichage vertical adapté aux écrans pivotés.;
Alterne entre le mode plein écran et le mode fenêtré.;
Enregistrer la configuration et quitter le menu.;
Annuler les modifications et quitter le menu.